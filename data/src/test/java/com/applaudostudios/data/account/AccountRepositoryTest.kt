package com.applaudostudios.data.account

import androidx.lifecycle.LiveData
import androidx.paging.DataSource
import androidx.paging.PagedList
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.dao.FavoriteDao
import com.applaudostudios.data.account.model.*
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.account.repository.AccountRepositoryImpl
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.login.model.SessionIdResponse
import com.applaudostudios.domain.account.model.Favorite
import com.applaudostudios.domain.login.model.SessionId
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class AccountRepositoryTest {

    private val accountApi = mock(AccountApi::class.java)
    private val accountDao = mock(AccountDao::class.java)
    private val loginDao = mock(LoginDao::class.java)
    private val favoriteDao = mock(FavoriteDao::class.java)
    private lateinit var accountRepository: AccountRepositoryImpl

    private val tmdb = Tmdb("https://image.com")
    private val gravatar = Gravatar("https://image.com")
    private val avatar = Avatar(gravatar, tmdb)
    private val accountWithAvatar = AccountWithAvatar(avatar, 1, "name", "username")

    private val accountTest = AccountResponse(1,
            "Name",
            "Username",
            accountWithAvatar.avatar.tmdb.avatarPath,
        accountWithAvatar.avatar.gravatar.hash)
    private val accountTestDomain = accountTest.mapToDomainModel()
    private val session = SessionIdResponse(true, "sessionId")
    private val favorite1 = FavoriteResponse("10-11-1998", 1, "Show 1", "Show 1", "https://showimage.com", 10.0)
    private val favorite2 = FavoriteResponse("04-04-1998", 2, "Show 2", "Show 2", "https://showimage.com", 10.0)
    private val favorite3 = FavoriteResponse("09-02-2007", 3, "Show 3", "Show 3", "https://showimage.com", 10.0)
    private val favoriteList = listOf(favorite1, favorite2, favorite3)
    private val favoritePreResponse = FavoritesPreResponse(1, favoriteList, 3, 60)

    @Before
    fun mockBehavior(){
        accountRepository = AccountRepositoryImpl(accountApi, accountDao, loginDao, favoriteDao)

        runBlocking {
            `when`(accountDao.getAccount()).thenReturn(accountTest)
            `when`(loginDao.getSession()).thenReturn(session)
        }
    }

    @Test
    fun getAccountInfo_completed_returnsAccountDomainModel(){
        runBlocking {
            val response = accountRepository.getAccountInfo()
            assertThat(response, IsEqual(accountTestDomain))
        }
    }

    /*@Test
    fun getFavorites_completed_returnsLivePagedList() {
        runBlocking {
            val response = accountRepository.getFavorites()
        }
    }*/

}