package com.applaudostudios.data.tvshow

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.model.AccountResponse
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.login.model.SessionIdResponse
import com.applaudostudios.data.season.dao.SeasonDao
import com.applaudostudios.data.season.model.SeasonResponse
import com.applaudostudios.data.tvshow.dao.*
import com.applaudostudios.data.tvshow.model.*
import com.applaudostudios.data.tvshow.model.relationship.TvShowCredits
import com.applaudostudios.data.tvshow.model.relationship.TvShowDetailsFull
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.data.tvshow.repository.TvShowDetailsRepositoryImpl
import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class TvShowDetailsRepositoryTest {

    private val tvShowApi = mock(TvShowApi::class.java)
    private val accountApi = mock(AccountApi::class.java)
    private val tvShowDetailsDao = mock(TvShowDetailsDao::class.java)
    private val createdByDao = mock(CreatedByDao::class.java)
    private val seasonDao = mock(SeasonDao::class.java)
    private val lastEpisodeToAirDao = mock(LastEpisodeToAirDao::class.java)
    private val tvShowCreditsDao = mock(TvShowCreditsPreResponseDao::class.java)
    private val tvShowCastDao = mock(TvShowCastDao::class.java)
    private val accountDao = mock(AccountDao::class.java)
    private val loginDao = mock(LoginDao::class.java)
    private lateinit var tvShowDetailsRepository: TvShowDetailsRepository

    private val tvShowId = 1

    private val tvShowDetailsDatabase = mutableListOf<TvShowDetailsResponse>()

    private val createdByDatabase = mutableListOf<CreatedByResponse>()
    private val createdBy = CreatedByResponse(1, "name", "path", 1)
    private val createdByList = mutableListOf<CreatedByResponse>(createdBy)

    private val lastEpisodeToAirDatabase = mutableListOf<LastEpisodeToAirResponse>()
    private val lastEpisodeToAir = LastEpisodeToAirResponse(1, 1, 1)

    private val seasonDatabase = mutableListOf<SeasonResponse>()
    private val season = SeasonResponse("airDate", 1, "name", "path", 1, 1)
    private val seasonList = mutableListOf(season)

    private val castDataBase = mutableListOf<CastResponse>()
    private val cast = CastResponse("char", 1, "name", "path", 1)
    private val castList = mutableListOf(cast)

    private val tvShowCreditsDatabase = mutableListOf<TvShowCreditsPreResponse>()
    private val tvShowCreditsPreResponse = TvShowCreditsPreResponse(1)

    private var favorite = false
    private val sessionIdBody = FavoriteBody(favorite, tvShowId, "tv")

    private val isFavoriteResponse = IsFavoriteResponse(true, 1)

    private val accountTest = AccountResponse(
        1,
        "name",
        "username",
        "path",
        "hash"
    )

    private val sessionTest = SessionIdResponse(true, "sessionId")

    private var tvShowDetailsResponse = TvShowDetailsResponse(
        "backdrop",
        "airDate",
        1,
        "name",
        1,
        "overview",
        "path",
        "ended",
        10.0
    )

    private var tvShowCredits = TvShowCredits(
        tvShowCreditsPreResponse,
        castList
    )

    private val tvShowDetailsFull = TvShowDetailsFull(
        tvShowDetailsResponse,
        createdByList,
        seasonList,
        lastEpisodeToAir
    )
    private val tvShowDetailsFullDomain = tvShowDetailsFull.mapToDomainModel()

    @Before
    fun mockBehavior() {

        tvShowDetailsRepository = TvShowDetailsRepositoryImpl(
            tvShowApi,
            accountApi,
            tvShowDetailsDao,
            createdByDao,
            seasonDao,
            lastEpisodeToAirDao,
            tvShowCreditsDao,
            tvShowCastDao,
            accountDao,
            loginDao
        )


        runBlocking {
            `when`(tvShowApi.getTvShowDetails(tvShowId, BuildConfig.API_KEY)).thenReturn(
                tvShowDetailsResponse
            )
            `when`(tvShowDetailsDao.insert(tvShowDetailsResponse)).then {
                tvShowDetailsDatabase.add(
                    tvShowDetailsResponse
                )
            }
            `when`(createdByDao.insertCreatedBy(createdByList)).then {
                createdByDatabase.addAll(
                    createdByList
                )
            }
            `when`(lastEpisodeToAirDao.insertLastEpisodeToAir(lastEpisodeToAir)).then {
                lastEpisodeToAirDatabase.add(
                    lastEpisodeToAir
                )
            }
            `when`(seasonDao.insertSeason(seasonList)).then { seasonDatabase.addAll(seasonList) }
            `when`(tvShowDetailsDao.getTvShowDetails(tvShowId)).thenReturn(tvShowDetailsFull)
            `when`(tvShowApi.getTvShowCredits(tvShowId, BuildConfig.API_KEY)).thenReturn(
                tvShowCreditsPreResponse
            )
            `when`(tvShowCreditsDao.insertTvShowCredits(tvShowCreditsPreResponse)).then {
                tvShowCreditsDatabase.add(
                    tvShowCreditsPreResponse
                )
            }
            `when`(tvShowCastDao.insertCast(castList)).then { castDataBase.addAll(castList) }
            `when`(tvShowCreditsDao.getTvShowCredits(tvShowId)).thenReturn(tvShowCredits)
            `when`(accountDao.getAccount()).thenReturn(accountTest)
            `when`(loginDao.getSession()).thenReturn(sessionTest)
            `when`(
                accountApi.markAsFavorite(
                    1,
                    BuildConfig.API_KEY,
                    sessionTest.sessionId,
                    sessionIdBody
                )
            ).then { kotlin.run { favorite = true } }
            `when`(tvShowApi.isFavorite(tvShowId, BuildConfig.API_KEY, sessionTest.sessionId)).thenReturn(isFavoriteResponse)
        }

    }

    @Test
    fun getTvShowDetails_completed_returnTvShowDetails() {
        runBlocking {
            val response = tvShowDetailsRepository.getTvShowDetails(tvShowId)
            tvShowDetailsFullDomain.cast = castList.map { it.mapToDomainModel() }
            assertThat(response, IsEqual(tvShowDetailsFullDomain))
        }
    }

    @Test
    fun markAsFavorite_completed() {
        runBlocking {
            tvShowDetailsRepository.markAsFavorite(tvShowId, true)
            assertThat(!favorite, IsEqual(true))
        }
    }

    @Test
    fun isFavorite_completed_returnsBoolean(){
        runBlocking {
            val response = tvShowDetailsRepository.isFavorite(tvShowId)
            assertThat(response, IsEqual(true))
        }
    }

}