package com.applaudostudios.data.login

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.model.*
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.login.model.DeleteSessionBody
import com.applaudostudios.data.login.model.RequestTokenResponse
import com.applaudostudios.data.login.model.SessionIdBody
import com.applaudostudios.data.login.model.SessionIdResponse
import com.applaudostudios.data.login.networking.LoginApi
import com.applaudostudios.data.login.repository.LoginRepositoryImpl
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock
import java.net.UnknownHostException

internal class LoginRepositoryTest {

    private val loginApi = mock(LoginApi::class.java)
    private val loginApiException = mock(LoginApi::class.java)
    private val loginDao = mock(LoginDao::class.java)
    private val accountApi = mock(AccountApi::class.java)
    private val accountDao = mock(AccountDao::class.java)
    private lateinit var loginRepository: LoginRepositoryImpl
    private lateinit var loginRepositoryException: LoginRepositoryImpl

    private val requestTokenResponse = RequestTokenResponse(true, "expires", "requestToken")
    private val requestToken = requestTokenResponse.mapToDomainModel()

    private val sessionDatabase = mutableListOf<SessionIdResponse>()
    private val sessionIdBody = SessionIdBody(requestTokenResponse.requestToken)
    private val accountDatabase = mutableListOf<AccountResponse>()
    private val accountResponse = AccountResponse(1, "name", "username", "https://image.com", "hash")
    private val sessionIdResponse = SessionIdResponse(true, "sessionId")
    private val sessionId = sessionIdResponse.mapToDomainModel()
    private val tmdb = Tmdb("https://image.com")
    private val gravatar = Gravatar("https://image.com")
    private val avatar = Avatar(gravatar, tmdb)
    private val accountWithAvatar = AccountWithAvatar(avatar, 1, "name", "username")
    private val deleteSessionBody = DeleteSessionBody(sessionId.sessionId)

    @Before
    fun mockBehavior(){

        loginRepository = LoginRepositoryImpl(loginApi, loginDao, accountApi, accountDao)
        loginRepositoryException = LoginRepositoryImpl(loginApiException, loginDao, accountApi, accountDao)


        runBlocking {
            `when`(loginApi.getRequestToken(BuildConfig.API_KEY)).thenReturn(requestTokenResponse)
            `when`(loginApiException.getRequestToken(BuildConfig.API_KEY)).then { throw UnknownHostException() }
            `when`(loginApi.getSessionId(BuildConfig.API_KEY, sessionIdBody)).thenReturn(sessionIdResponse)
            `when`(loginDao.deleteSession()).then { sessionDatabase.clear() }
            `when`(loginDao.insertSession(sessionIdResponse)).then { sessionDatabase.add(sessionIdResponse) }
            `when`(accountApi.getAccountDetails(BuildConfig.API_KEY, sessionIdResponse.sessionId)).thenReturn(accountWithAvatar)
            `when`(accountDao.deleteAccount()).then { accountDatabase.clear() }
            `when`(accountDao.insertAccount(accountResponse)).then { accountDatabase.add(accountResponse) }
            `when`(loginDao.getSession()).thenReturn(sessionIdResponse)
        }

    }

    @Test
    fun requestToken_completed_returnRequestToken(){
        runBlocking {
            val response = loginRepository.requestToken()
            assertThat(response, IsEqual(requestToken))
        }
    }

    @Test
    fun requestToken_noCompleted_returnRequestToken(){
        runBlocking {
            val response = loginRepositoryException.requestToken()
            assertThat(response, IsEqual(null))
        }
    }

    @Test
    fun requestSessionId_completed_returnBoolean(){
        runBlocking {
            val response = loginRepository.requestSessionId(requestToken.requestToken)
            assertThat(response, IsEqual(true))
            assertThat(!accountDatabase.contains(accountResponse), IsEqual(true))
            assertThat(sessionDatabase.contains(sessionIdResponse), IsEqual(true))
        }
    }

    @Test
    fun getSessionId_completed_returnSessionId(){
        runBlocking {
            val response = loginRepository.getSessionId()
            assertThat(response, IsEqual(sessionId))
        }
    }

    @Test
    fun deleteSession_completed(){
        runBlocking {
            loginRepository.deleteSession()
            assertThat(sessionDatabase.isEmpty(), IsEqual(true))
        }
    }

}