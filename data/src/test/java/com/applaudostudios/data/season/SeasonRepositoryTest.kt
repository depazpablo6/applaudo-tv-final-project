package com.applaudostudios.data.season

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.season.dao.EpisodeDao
import com.applaudostudios.data.season.dao.SeasonDao
import com.applaudostudios.data.season.model.EpisodeResponse
import com.applaudostudios.data.season.model.SeasonPreResponse
import com.applaudostudios.data.season.model.SeasonResponse
import com.applaudostudios.data.season.networking.SeasonApi
import com.applaudostudios.data.season.repository.SeasonRepositoryImpl
import com.applaudostudios.domain.season.model.SeasonWithEpisodes
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class SeasonRepositoryTest {

    private val tvShowId = 1
    private val seasonNumber = 1
    private val seasonDao = mock(SeasonDao::class.java)
    private val episodeDao = mock(EpisodeDao::class.java)
    private val seasonApi = mock(SeasonApi::class.java)
    private lateinit var seasonRepository: SeasonRepositoryImpl

    private val seasonDatabase = mutableListOf<SeasonResponse>()
    private val season = SeasonResponse("airdate", 1, "name", "path", 1, 1)
    private val seasonList = mutableListOf(season)

    private val episodeDatabase = mutableListOf<EpisodeResponse>()
    private val episode = EpisodeResponse(
        "airDate",
        1,
        1,
        "name",
        "overview",
        "path",
        10.0,
        seasonNumber,
        1
    )
    private val episodeList = mutableListOf(episode)
    private val seasonPreResponse = SeasonPreResponse("1", episodeList)
    private val seasonWithEpisodes = SeasonWithEpisodes(seasonNumber, episodeList.map { it.mapToDomainModel() })
    private val listSeasonWithEpisodes = mutableListOf(seasonWithEpisodes)

    @Before
    fun mockBehavior(){
        seasonRepository = SeasonRepositoryImpl(seasonDao, episodeDao, seasonApi)
        runBlocking {
            `when`(seasonDao.getSeason(tvShowId)).thenReturn(seasonList)
            `when`(seasonApi.getSeasonEpisodes(tvShowId, seasonNumber, BuildConfig.API_KEY)).thenReturn(seasonPreResponse)
            `when`(episodeDao.insertAll(episodeList)).then { episodeDatabase.addAll(episodeList) }
            `when`(episodeDao.getAllEpisodes(seasonNumber, tvShowId)).thenReturn(episodeDatabase)
        }
    }

    @Test
    fun getAllSeasons_completed_returnsListOfSeasonsWithEpisodes(){
        runBlocking {
            val response = seasonRepository.getAllSeasons(tvShowId)
            assertThat(response, IsEqual(listSeasonWithEpisodes))
        }
    }

}