package com.applaudostudios.data.common.mapper

interface DomainMapper<T : Any> {
    fun mapToDomainModel(): T
}