package com.applaudostudios.data.common.di

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.applaudostudios.data.account.repository.AccountRepositoryImpl
import com.applaudostudios.data.login.repository.LoginRepositoryImpl
import com.applaudostudios.data.season.repository.SeasonRepositoryImpl
import com.applaudostudios.data.tvshow.repository.TvShowDetailsRepositoryImpl
import com.applaudostudios.data.tvshow.repository.TvShowRepositoryImpl
import com.applaudostudios.domain.account.model.Favorite
import com.applaudostudios.domain.account.repository.AccountRepository
import com.applaudostudios.domain.login.repository.LoginRepository
import com.applaudostudios.domain.season.repository.SeasonRepository
import com.applaudostudios.domain.tvshow.model.TvShow
import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository
import com.applaudostudios.domain.tvshow.repository.TvShowRepository
import org.koin.dsl.module

val repositoryModule = module {
    factory<LoginRepository> { LoginRepositoryImpl(loginApi = get(), loginDao = get(), accountApi = get(), accountDao = get()) }
    factory<TvShowRepository<LiveData<PagedList<TvShow>>>> {
        TvShowRepositoryImpl(
                tvShowApi = get(),
                applaudoTvDatabase = get()
        )
    }
    factory<TvShowDetailsRepository> {
        TvShowDetailsRepositoryImpl(
                tvShowApi = get(),
                tvShowDetailsDao = get(),
                createdByDao = get(),
                seasonDao = get(),
                lastEpisodeToAirDao = get(),
                tvShowCreditsDao = get(),
                tvShowCastDao = get(),
                accountApi = get(),
                accountDao = get(),
                loginDao = get()
        )
    }

    factory<SeasonRepository> {
        SeasonRepositoryImpl(
                seasonDao = get(),
                episodeDao = get(),
                seasonApi = get()
        )
    }

    factory<AccountRepository<LiveData<PagedList<Favorite>>>> {
        AccountRepositoryImpl(
                accountApi = get(),
                loginDao = get(),
                favoriteDao = get(),
                accountDao = get()
        )
    }

}