package com.applaudostudios.data.common.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.dao.FavoriteDao
import com.applaudostudios.data.account.model.AccountResponse
import com.applaudostudios.data.account.model.FavoriteResponse
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.login.model.SessionIdResponse
import com.applaudostudios.data.season.dao.EpisodeDao
import com.applaudostudios.data.season.dao.SeasonDao
import com.applaudostudios.data.season.model.EpisodeResponse
import com.applaudostudios.data.season.model.SeasonResponse
import com.applaudostudios.data.tvshow.dao.*
import com.applaudostudios.data.tvshow.model.*

@Database(
        entities = [
            SessionIdResponse::class,
            TvShowResponse::class,
            CreatedByResponse::class,
            LastEpisodeToAirResponse::class,
            SeasonResponse::class,
            TvShowDetailsResponse::class,
            TvShowCreditsPreResponse::class,
            CastResponse::class,
            AccountResponse::class,
            EpisodeResponse::class,
            FavoriteResponse::class],
        version = 1,
        exportSchema = false
)
abstract class ApplaudoTvDatabase : RoomDatabase() {

    abstract fun loginDao(): LoginDao
    abstract fun tvShowDao(): TvShowDao
    abstract fun lastEpisodeToAirDao(): LastEpisodeToAirDao
    abstract fun seasonDao(): SeasonDao
    abstract fun createdByDao(): CreatedByDao
    abstract fun tvShowDetailsDao(): TvShowDetailsDao
    abstract fun tvShowCreditsDao(): TvShowCreditsPreResponseDao
    abstract fun tvShowCastDao(): TvShowCastDao
    abstract fun accountDao(): AccountDao
    abstract fun episodeDao(): EpisodeDao
    abstract fun favoriteDao(): FavoriteDao

    companion object {
        fun getDatabase(context: Context) = Room.databaseBuilder(
                context.applicationContext,
                ApplaudoTvDatabase::class.java,
                "applaudotv_database"
        ).build()
    }
}