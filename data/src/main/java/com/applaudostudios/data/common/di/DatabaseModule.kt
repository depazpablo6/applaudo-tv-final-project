package com.applaudostudios.data.common.di

import com.applaudostudios.data.common.database.ApplaudoTvDatabase
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val databaseModule = module {
    single { ApplaudoTvDatabase.getDatabase(androidContext()) }
    factory { get<ApplaudoTvDatabase>().loginDao() }
    factory { get<ApplaudoTvDatabase>().tvShowDao() }
    factory { get<ApplaudoTvDatabase>().tvShowDetailsDao() }
    factory { get<ApplaudoTvDatabase>().createdByDao() }
    factory { get<ApplaudoTvDatabase>().lastEpisodeToAirDao() }
    factory { get<ApplaudoTvDatabase>().seasonDao() }
    factory { get<ApplaudoTvDatabase>().tvShowCreditsDao() }
    factory { get<ApplaudoTvDatabase>().tvShowCastDao() }
    factory { get<ApplaudoTvDatabase>().accountDao() }
    factory { get<ApplaudoTvDatabase>().episodeDao() }
    factory { get<ApplaudoTvDatabase>().favoriteDao() }
}
