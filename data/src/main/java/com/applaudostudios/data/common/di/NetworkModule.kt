package com.applaudostudios.data.common.di

import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.networking.LoginApi
import com.applaudostudios.data.season.networking.SeasonApi
import com.applaudostudios.data.tvshow.networking.TvShowApi
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

private const val BASE_URL = "https://api.themoviedb.org/3/"

val networkModule = module {

    single {
        Retrofit.Builder()
                .baseUrl(BASE_URL)
                .addConverterFactory(get())
                .build()
    }

    single { GsonConverterFactory.create() as Converter.Factory }

    single { get<Retrofit>().create(LoginApi::class.java) }
    single { get<Retrofit>().create(TvShowApi::class.java) }
    single { get<Retrofit>().create(AccountApi::class.java) }
    single { get<Retrofit>().create(SeasonApi::class.java) }

}
