package com.applaudostudios.data.tvshow.model


import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.tvshow.model.LastEpisodeToAir
import com.google.gson.annotations.SerializedName

@Entity(
        foreignKeys = [ForeignKey(
                entity = TvShowDetailsResponse::class,
                parentColumns = ["idDetails"],
                childColumns = ["detailsId"],
                onDelete = ForeignKey.CASCADE
        )]
)
data class LastEpisodeToAirResponse(
        @SerializedName("id")
        @PrimaryKey
        val idLastEpisode: Int,
        @SerializedName("season_number")
        val seasonNumber: Int,
        var detailsId: Int
) : DomainMapper<LastEpisodeToAir> {
    override fun mapToDomainModel() = LastEpisodeToAir(idLastEpisode, seasonNumber, detailsId)
}