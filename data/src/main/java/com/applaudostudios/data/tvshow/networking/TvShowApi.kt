package com.applaudostudios.data.tvshow.networking

import com.applaudostudios.data.tvshow.model.IsFavoriteResponse
import com.applaudostudios.data.tvshow.model.TvShowCreditsPreResponse
import com.applaudostudios.data.tvshow.model.TvShowDetailsResponse
import com.applaudostudios.data.tvshow.model.TvShowPreResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface TvShowApi {

    @GET("tv/airing_today")
    suspend fun getAiringTodayTvShows(
            @Query("api_key") key: String,
            @Query("page") page: Int
    ): TvShowPreResponse

    @GET("tv/on_the_air")
    suspend fun getOnAirTvShows(
            @Query("api_key") key: String,
            @Query("page") page: Int
    ): TvShowPreResponse

    @GET("tv/popular")
    suspend fun getPopularTvShows(
            @Query("api_key") key: String,
            @Query("page") page: Int
    ): TvShowPreResponse

    @GET("tv/top_rated")
    suspend fun getTopRatedTvShows(
            @Query("api_key") key: String,
            @Query("page") page: Int
    ): TvShowPreResponse

    @GET("tv/{tv_id}")
    suspend fun getTvShowDetails(
            @Path("tv_id") tvShowId: Int,
            @Query("api_key") key: String
    ): TvShowDetailsResponse

    @GET("tv/{tv_id}/credits")
    suspend fun getTvShowCredits(
            @Path("tv_id") tvShowId: Int,
            @Query("api_key") key: String
    ): TvShowCreditsPreResponse

    @GET("tv/{tv_id}/account_states")
    suspend fun isFavorite(
            @Path("tv_id") tvShowId: Int,
            @Query("api_key") key: String,
            @Query("session_id") session: String
    ): IsFavoriteResponse

    companion object {
        const val FIRST_PAGE = 1
        const val NETWORK_PAGE = 20
        const val PRELOAD_DISTANCE = 10
    }

}