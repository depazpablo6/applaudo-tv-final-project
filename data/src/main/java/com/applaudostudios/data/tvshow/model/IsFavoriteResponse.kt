package com.applaudostudios.data.tvshow.model


import com.google.gson.annotations.SerializedName

data class IsFavoriteResponse(
        @SerializedName("favorite")
        val favorite: Boolean,
        @SerializedName("id")
        val id: Int,
)