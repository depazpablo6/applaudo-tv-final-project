package com.applaudostudios.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.tvshow.model.TvShowCreditsPreResponse
import com.applaudostudios.data.tvshow.model.relationship.TvShowCredits

@Dao
interface TvShowCreditsPreResponseDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertTvShowCredits(tvShowCreditsPreResponse: TvShowCreditsPreResponse)

    @Query("SELECT * FROM TvShowCreditsPreResponse WHERE idCredits = :id")
    suspend fun getTvShowCredits(id: Int): TvShowCredits?

}