package com.applaudostudios.data.tvshow.repository

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.applaudostudios.data.common.database.ApplaudoTvDatabase
import com.applaudostudios.data.tvshow.datasource.TvShowsBoundaryCallback
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.data.tvshow.networking.TvShowApi.Companion.NETWORK_PAGE
import com.applaudostudios.data.tvshow.networking.TvShowApi.Companion.PRELOAD_DISTANCE
import com.applaudostudios.domain.tvshow.model.TvShow
import com.applaudostudios.domain.tvshow.repository.TvShowRepository
import com.applaudostudios.domain.tvshow.util.TvShowFilter

class TvShowRepositoryImpl(
        private val tvShowApi: TvShowApi,
        private val applaudoTvDatabase: ApplaudoTvDatabase
) : TvShowRepository<LiveData<PagedList<TvShow>>> {


    override suspend fun getTvShows(filter: TvShowFilter): LiveData<PagedList<TvShow>> {

        val config = PagedList.Config.Builder()
                .setPageSize(NETWORK_PAGE)
                .setInitialLoadSizeHint(NETWORK_PAGE)
                .setPrefetchDistance(PRELOAD_DISTANCE)
                .setEnablePlaceholders(false)
                .build()

        return LivePagedListBuilder(
                applaudoTvDatabase.tvShowDao().getTvShows().mapByPage { list ->
                    list.map {
                        it.mapToDomainModel()
                    }
                },
                config
        ).setBoundaryCallback(TvShowsBoundaryCallback(tvShowApi, applaudoTvDatabase, filter))
                .build()
    }
}