package com.applaudostudios.data.tvshow.model


import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.tvshow.model.Cast
import com.google.gson.annotations.SerializedName

@Entity(
        foreignKeys = [ForeignKey(
                entity = TvShowCreditsPreResponse::class,
                parentColumns = ["idCredits"],
                childColumns = ["creditsId"],
                onDelete = ForeignKey.CASCADE
        )]
)
data class CastResponse(
        @SerializedName("character")
        val character: String,
        @SerializedName("id")
        @PrimaryKey
        val id: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("profile_path")
        val profilePath: String,
        var creditsId: Int
) : DomainMapper<Cast> {
    override fun mapToDomainModel() = Cast(
            character, id, name, profilePath, creditsId
    )
}