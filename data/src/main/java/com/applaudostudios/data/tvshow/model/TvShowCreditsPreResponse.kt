package com.applaudostudios.data.tvshow.model

import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName

@Entity
data class TvShowCreditsPreResponse(
        @SerializedName("id")
        @PrimaryKey
        val idCredits: Int
) {
    @Ignore
    @SerializedName("cast")
    val cast: List<CastResponse>? = null
}