package com.applaudostudios.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.tvshow.model.TvShowDetailsResponse
import com.applaudostudios.data.tvshow.model.relationship.TvShowDetailsFull

@Dao
interface TvShowDetailsDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insert(tvShowDetails: TvShowDetailsResponse)

    @Query("SELECT * FROM TvShowDetailsResponse WHERE idDetails = :id")
    suspend fun getTvShowDetails(id: Int): TvShowDetailsFull?

}