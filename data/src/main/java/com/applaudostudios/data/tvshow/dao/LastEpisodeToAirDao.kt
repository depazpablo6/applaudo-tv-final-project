package com.applaudostudios.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.tvshow.model.LastEpisodeToAirResponse

@Dao
interface LastEpisodeToAirDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertLastEpisodeToAir(lastEpisodeToAirResponse: LastEpisodeToAirResponse)

    @Query("SELECT * FROM LastEpisodeToAirResponse WHERE idLastEpisode = :id")
    suspend fun getLastEpisodeToAir(id: Int): LastEpisodeToAirResponse

}