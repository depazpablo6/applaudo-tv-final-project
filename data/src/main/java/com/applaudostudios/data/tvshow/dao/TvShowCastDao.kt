package com.applaudostudios.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import com.applaudostudios.data.tvshow.model.CastResponse

@Dao
interface TvShowCastDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCast(cast: List<CastResponse>)

}