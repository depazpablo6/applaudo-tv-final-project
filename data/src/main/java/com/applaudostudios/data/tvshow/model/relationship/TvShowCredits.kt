package com.applaudostudios.data.tvshow.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudostudios.data.tvshow.model.CastResponse
import com.applaudostudios.data.tvshow.model.TvShowCreditsPreResponse

data class TvShowCredits(
        @Embedded val tvShowCreditsPreResponse: TvShowCreditsPreResponse,
        @Relation(
                parentColumn = "idCredits",
                entityColumn = "creditsId"
        ) val cast: List<CastResponse>
)