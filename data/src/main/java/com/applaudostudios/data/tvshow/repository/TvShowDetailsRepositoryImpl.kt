package com.applaudostudios.data.tvshow.repository

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.season.dao.SeasonDao
import com.applaudostudios.data.tvshow.dao.*
import com.applaudostudios.data.tvshow.model.FavoriteBody
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.domain.tvshow.model.Cast
import com.applaudostudios.domain.tvshow.model.TvShowDetails
import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository
import java.net.UnknownHostException

class TvShowDetailsRepositoryImpl(
        private val tvShowApi: TvShowApi,
        private val accountApi: AccountApi,
        private val tvShowDetailsDao: TvShowDetailsDao,
        private val createdByDao: CreatedByDao,
        private val seasonDao: SeasonDao,
        private val lastEpisodeToAirDao: LastEpisodeToAirDao,
        private val tvShowCreditsDao: TvShowCreditsPreResponseDao,
        private val tvShowCastDao: TvShowCastDao,
        private val accountDao: AccountDao,
        private val loginDao: LoginDao
) : TvShowDetailsRepository {
    override suspend fun getTvShowDetails(tvShowId: Int): TvShowDetails? {
        try {
            val response = tvShowApi.getTvShowDetails(tvShowId, BuildConfig.API_KEY)
            tvShowDetailsDao.insert(response)
            response.createdBy?.let { it ->
                it.map { creator ->
                    creator.detailsId = response.idDetails
                }
                createdByDao.insertCreatedBy(it)
            }
            response.lastEpisodeToAir?.let {
                it.detailsId = response.idDetails
                lastEpisodeToAirDao.insertLastEpisodeToAir(it)
            }
            response.seasons?.let {
                it.map { season ->
                    season.detailsId = response.idDetails
                }
                seasonDao.insertSeason(it)
            }
        } catch (e: UnknownHostException) {
        }
        val details = tvShowDetailsDao.getTvShowDetails(tvShowId)?.mapToDomainModel()
        details?.cast = details?.id?.let { getCredits(it) }
        return details
    }

    override suspend fun markAsFavorite(tvShowId: Int, favorite: Boolean) {
        try {
            val body = FavoriteBody(favorite, tvShowId, "tv")
            val accountId = accountDao.getAccount().id
            val sessionId = loginDao.getSession().sessionId
            accountApi.markAsFavorite(accountId, BuildConfig.API_KEY, sessionId, body)
        } catch (e: UnknownHostException) {
        }
    }

    override suspend fun isFavorite(tvShowId: Int): Boolean {
        return try {
            val sessionId = loginDao.getSession().sessionId
            tvShowApi.isFavorite(tvShowId, BuildConfig.API_KEY, sessionId).favorite
        } catch (e: UnknownHostException) {
            false
        }
    }


    private suspend fun getCredits(id: Int): List<Cast>? {
        try {
            val response = tvShowApi.getTvShowCredits(id, BuildConfig.API_KEY)
            tvShowCreditsDao.insertTvShowCredits(response)
            response.cast?.let {
                it.map { cast -> cast.creditsId = response.idCredits }
                tvShowCastDao.insertCast(it)
            }
        } catch (e: UnknownHostException) {
        }
        return tvShowCreditsDao.getTvShowCredits(id)?.cast?.map { it.mapToDomainModel() }
    }

}