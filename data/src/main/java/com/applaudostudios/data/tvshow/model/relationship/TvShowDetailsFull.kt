package com.applaudostudios.data.tvshow.model.relationship

import androidx.room.Embedded
import androidx.room.Relation
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.data.season.model.SeasonResponse
import com.applaudostudios.data.tvshow.model.CreatedByResponse
import com.applaudostudios.data.tvshow.model.LastEpisodeToAirResponse
import com.applaudostudios.data.tvshow.model.TvShowDetailsResponse
import com.applaudostudios.domain.tvshow.model.TvShowDetails

data class TvShowDetailsFull(
        @Embedded val tvShowDetailsResponse: TvShowDetailsResponse,
        @Relation(
                parentColumn = "idDetails",
                entityColumn = "detailsId"
        )
        val creators: List<CreatedByResponse>,
        @Relation(
                parentColumn = "idDetails",
                entityColumn = "detailsId"
        )
        val seasons: List<SeasonResponse>,
        @Relation(
                parentColumn = "idDetails",
                entityColumn = "detailsId"
        )
        val lastEpisodeToAirResponse: LastEpisodeToAirResponse?,

        ) : DomainMapper<TvShowDetails> {
    override fun mapToDomainModel() = TvShowDetails(
            tvShowDetailsResponse.backdropPath,
            creators.map { it.mapToDomainModel() },
            tvShowDetailsResponse.firstAirDate,
            tvShowDetailsResponse.idDetails,
            lastEpisodeToAirResponse?.mapToDomainModel(),
            tvShowDetailsResponse.name,
            tvShowDetailsResponse.numberOfSeasons,
            tvShowDetailsResponse.overview,
            tvShowDetailsResponse.posterPath,
            seasons.map { it.mapToDomainModel() },
            tvShowDetailsResponse.status,
            tvShowDetailsResponse.voteAverage,
            null
    )
}