package com.applaudostudios.data.tvshow.model


import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.applaudostudios.data.season.model.SeasonResponse
import com.google.gson.annotations.SerializedName

@Entity
data class TvShowDetailsResponse(
        @SerializedName("backdrop_path")
        val backdropPath: String?,
        @SerializedName("first_air_date")
        val firstAirDate: String,
        @SerializedName("id")
        @PrimaryKey
        val idDetails: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("number_of_seasons")
        val numberOfSeasons: Int,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("poster_path")
        val posterPath: String,
        @SerializedName("status")
        val status: String,
        @SerializedName("vote_average")
        val voteAverage: Double,
) {
    @Ignore
    @SerializedName("created_by")
    val createdBy: List<CreatedByResponse>? = null

    @Ignore
    @SerializedName("last_episode_to_air")
    val lastEpisodeToAir: LastEpisodeToAirResponse? = null

    @Ignore
    @SerializedName("seasons")
    val seasons: List<SeasonResponse>? = null
}