package com.applaudostudios.data.tvshow.model


import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.tvshow.model.CreatedBy
import com.google.gson.annotations.SerializedName

@Entity(
        foreignKeys = [
            ForeignKey(
                    entity = TvShowDetailsResponse::class,
                    parentColumns = ["idDetails"],
                    childColumns = ["detailsId"],
                    onDelete = CASCADE
            )
        ]
)
data class CreatedByResponse(
        @SerializedName("id")
        @PrimaryKey
        val idCreator: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("profile_path")
        val profilePath: String?,
        var detailsId: Int
) : DomainMapper<CreatedBy> {
    override fun mapToDomainModel() = CreatedBy(idCreator, name, profilePath, detailsId)
}