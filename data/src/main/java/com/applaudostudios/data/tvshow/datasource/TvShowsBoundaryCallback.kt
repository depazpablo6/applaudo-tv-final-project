package com.applaudostudios.data.tvshow.datasource

import android.util.Log
import androidx.paging.PagedList
import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.common.database.ApplaudoTvDatabase
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.data.tvshow.networking.TvShowApi.Companion.FIRST_PAGE
import com.applaudostudios.domain.tvshow.model.TvShow
import com.applaudostudios.domain.tvshow.util.TvShowFilter
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import java.io.IOException

class TvShowsBoundaryCallback(
        private val tvShowsApi: TvShowApi,
        private val applaudoTvDatabase: ApplaudoTvDatabase,
        private val filter: TvShowFilter
) : PagedList.BoundaryCallback<TvShow>() {

    private var currentPage = FIRST_PAGE

    init {
        firstPopulate()
    }

    private fun firstPopulate() {
        runBlocking {
            try {
                val response = filterResponse(filter, FIRST_PAGE)
                applaudoTvDatabase.tvShowDao().filterTvShows(response.results)
            } catch (e: IOException) {
                //handle db exception
            } catch (e: HttpException) {
                //handle db exception
            }
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: TvShow) {
        runBlocking {
            try {
                val response = filterResponse(filter, currentPage.plus(1))
                currentPage = response.page
                applaudoTvDatabase.tvShowDao().insertAll(response.results)
            } catch (e: IOException) {
                Log.i("ERROR DE BASE: ", "ERROR AL INSERTAR EN LA BASE")
            } catch (e: HttpException) {
                Log.i("ERROR DE NETWORK: ", "ERROR AL INSERTAR EN LA BASE")
            }
        }
    }

    private suspend fun filterResponse(filter: TvShowFilter, page: Int) = when (filter) {
        TvShowFilter.ON_AIR -> tvShowsApi.getOnAirTvShows(BuildConfig.API_KEY, page)
        TvShowFilter.AIRING_TODAY -> tvShowsApi.getAiringTodayTvShows(BuildConfig.API_KEY, page)
        TvShowFilter.POPULAR -> tvShowsApi.getPopularTvShows(BuildConfig.API_KEY, page)
        TvShowFilter.TOP_RATED -> tvShowsApi.getTopRatedTvShows(BuildConfig.API_KEY, page)
    }

}