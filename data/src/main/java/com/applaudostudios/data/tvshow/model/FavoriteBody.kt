package com.applaudostudios.data.tvshow.model


import com.google.gson.annotations.SerializedName

data class FavoriteBody(
        @SerializedName("favorite")
        val favorite: Boolean,
        @SerializedName("media_id")
        val mediaId: Int,
        @SerializedName("media_type")
        val mediaType: String
)