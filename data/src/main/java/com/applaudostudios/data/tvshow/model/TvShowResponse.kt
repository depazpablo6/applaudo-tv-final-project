package com.applaudostudios.data.tvshow.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.tvshow.model.TvShow
import com.google.gson.annotations.SerializedName

@Entity
data class TvShowResponse(
        @SerializedName("first_air_date")
        val firstAirDate: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        @PrimaryKey
        val name: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("poster_path")
        val posterPath: String?,
        @SerializedName("vote_average")
        val voteAverage: Double,
) : DomainMapper<TvShow> {
    override fun mapToDomainModel() = TvShow(
            firstAirDate, id, name, overview, posterPath, voteAverage
    )
}