package com.applaudostudios.data.tvshow.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.tvshow.model.CreatedByResponse

@Dao
interface CreatedByDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertCreatedBy(createdByResponse: List<CreatedByResponse>)

    @Query("SELECT * FROM CreatedByResponse WHERE idCreator = :id")
    suspend fun getCreatedBy(id: Int): CreatedByResponse

}