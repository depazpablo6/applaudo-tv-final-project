package com.applaudostudios.data.tvshow.model

import com.google.gson.annotations.SerializedName

data class TvShowPreResponse(
        @SerializedName("page") val page: Int,
        @SerializedName("results") val results: List<TvShowResponse>,
        @SerializedName("total_results") val totalResults: Int,
        @SerializedName("total_pages") val totalPages: Int
)