package com.applaudostudios.data.tvshow.dao

import androidx.paging.DataSource
import androidx.room.*
import com.applaudostudios.data.tvshow.model.TvShowResponse

@Dao
interface TvShowDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(tvShows: List<TvShowResponse>)

    @Query("DELETE FROM TvShowResponse")
    suspend fun deleteAll()

    @Query("SELECT * FROM TvShowResponse")
    fun getTvShows(): DataSource.Factory<Int, TvShowResponse>

    @Query("SELECT COUNT(*) FROM TvShowResponse")
    suspend fun getTvShowCount(): Int

    @Transaction
    suspend fun filterTvShows(tvShows: List<TvShowResponse>) {
        deleteAll()
        insertAll(tvShows)
    }

}