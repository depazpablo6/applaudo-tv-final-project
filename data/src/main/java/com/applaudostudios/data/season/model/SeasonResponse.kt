package com.applaudostudios.data.season.model


import androidx.room.Entity
import androidx.room.ForeignKey
import androidx.room.ForeignKey.CASCADE
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.data.tvshow.model.TvShowDetailsResponse
import com.applaudostudios.domain.season.model.Season
import com.google.gson.annotations.SerializedName

@Entity(
        foreignKeys = [ForeignKey(
                entity = TvShowDetailsResponse::class,
                parentColumns = ["idDetails"],
                childColumns = ["detailsId"],
                onDelete = CASCADE
        )]
)
data class SeasonResponse(
        @SerializedName("air_date")
        val airDate: String,
        @SerializedName("id")
        @PrimaryKey
        val idSeason: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("poster_path")
        val posterPath: String?,
        @SerializedName("season_number")
        val seasonNumber: Int,
        var detailsId: Int
) : DomainMapper<Season> {
    override fun mapToDomainModel() =
            Season(airDate, idSeason, name, posterPath, seasonNumber, detailsId)
}