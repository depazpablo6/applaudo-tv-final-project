package com.applaudostudios.data.season.repository

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.season.dao.EpisodeDao
import com.applaudostudios.data.season.dao.SeasonDao
import com.applaudostudios.data.season.networking.SeasonApi
import com.applaudostudios.domain.season.model.SeasonWithEpisodes
import com.applaudostudios.domain.season.repository.SeasonRepository
import java.net.UnknownHostException

class SeasonRepositoryImpl(
        private val seasonDao: SeasonDao,
        private val episodeDao: EpisodeDao,
        private val seasonApi: SeasonApi
) : SeasonRepository {

    override suspend fun getAllSeasons(tvShowId: Int): List<SeasonWithEpisodes> {
        val seasonsAndEpisodes = mutableListOf<SeasonWithEpisodes>()
        val seasons = seasonDao.getSeason(tvShowId)
        seasons.forEach {
            try {
                val response = seasonApi.getSeasonEpisodes(tvShowId, it.seasonNumber, BuildConfig.API_KEY)
                response.episodes.map { episode -> episode.tvShowId = tvShowId }
                episodeDao.insertAll(response.episodes)
            } catch (e: UnknownHostException) {
            }
            val episodes = episodeDao.getAllEpisodes(it.seasonNumber, tvShowId)
            seasonsAndEpisodes.add(SeasonWithEpisodes(
                    it.seasonNumber,
                    episodes.map { episode -> episode.mapToDomainModel() }
            ))
        }
        return seasonsAndEpisodes
    }

}