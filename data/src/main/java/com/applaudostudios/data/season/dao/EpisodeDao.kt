package com.applaudostudios.data.season.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.season.model.EpisodeResponse

@Dao
interface EpisodeDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(episodes: List<EpisodeResponse>)

    @Query("SELECT * FROM EpisodeResponse WHERE seasonNumber = :seasonNumber AND tvShowId = :tvShowId")
    suspend fun getAllEpisodes(seasonNumber: Int, tvShowId: Int): List<EpisodeResponse>

}