package com.applaudostudios.data.season.networking

import com.applaudostudios.data.season.model.SeasonPreResponse
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface SeasonApi {

    @GET("tv/{tv_id}/season/{season_number}")
    suspend fun getSeasonEpisodes(
            @Path("tv_id") tvShowId: Int,
            @Path("season_number") seasonNumber: Int,
            @Query("api_key") key: String
    ): SeasonPreResponse

}