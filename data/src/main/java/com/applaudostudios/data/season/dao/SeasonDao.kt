package com.applaudostudios.data.season.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.season.model.SeasonResponse

@Dao
interface SeasonDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertSeason(season: List<SeasonResponse>)

    @Query("SELECT * FROM SeasonResponse WHERE detailsId = :tvShowId")
    suspend fun getSeason(tvShowId: Int): List<SeasonResponse>

}