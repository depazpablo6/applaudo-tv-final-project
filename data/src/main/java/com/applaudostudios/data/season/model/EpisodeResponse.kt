package com.applaudostudios.data.season.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.season.model.Episode
import com.google.gson.annotations.SerializedName

@Entity
data class EpisodeResponse(
        @SerializedName("air_date")
        val airDate: String,
        @SerializedName("episode_number")
        val episodeNumber: Int,
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        @PrimaryKey
        val name: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("still_path")
        val stillPath: String,
        @SerializedName("vote_average")
        val voteAverage: Double,
        @SerializedName("season_number")
        val seasonNumber: Int,
        var tvShowId: Int = 0
) : DomainMapper<Episode> {
    override fun mapToDomainModel() = Episode(
            airDate, episodeNumber, id, name, overview, stillPath, voteAverage, seasonNumber, tvShowId
    )

}