package com.applaudostudios.data.season.model

import com.google.gson.annotations.SerializedName

data class SeasonPreResponse(
        @SerializedName("_id")
        val seasonId: String,
        @SerializedName("episodes")
        val episodes: List<EpisodeResponse>
)
