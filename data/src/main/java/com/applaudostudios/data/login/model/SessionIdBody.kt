package com.applaudostudios.data.login.model

import com.google.gson.annotations.SerializedName

data class SessionIdBody(
        @SerializedName("request_token") val request_token: String
)