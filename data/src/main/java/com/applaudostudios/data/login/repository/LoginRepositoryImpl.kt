package com.applaudostudios.data.login.repository

import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.model.AccountResponse
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.login.model.SessionIdBody
import com.applaudostudios.data.login.networking.LoginApi
import com.applaudostudios.domain.login.model.SessionId
import com.applaudostudios.domain.login.repository.LoginRepository
import java.net.UnknownHostException

class LoginRepositoryImpl(
        private val loginApi: LoginApi,
        private val loginDao: LoginDao,
        private val accountApi: AccountApi,
        private val accountDao: AccountDao)
    : LoginRepository {

    override suspend fun requestToken() = try {
        loginApi.getRequestToken(BuildConfig.API_KEY).mapToDomainModel()
    } catch (e: UnknownHostException) {
        null
    }


    override suspend fun requestSessionId(requestToken: String): Boolean {
        try {
            val sessionId = loginApi.getSessionId(BuildConfig.API_KEY, SessionIdBody(requestToken))
            loginDao.deleteSession()
            loginDao.insertSession(sessionId)
            val response = accountApi.getAccountDetails(BuildConfig.API_KEY, sessionId.sessionId)
            val account = AccountResponse(
                    response.id,
                    response.name,
                    response.username,
                    response.avatar.tmdb.avatarPath,
                    response.avatar.gravatar.hash)
            accountDao.deleteAccount()
            accountDao.insertAccount(account)
            return true
        } catch (e: UnknownHostException) {
            return false
        }
    }

    override suspend fun getSessionId(): SessionId? = try {
        loginDao.getSession().mapToDomainModel()
    } catch (e: Exception) {
        null
    }

    override suspend fun deleteSession() {
        /*val sessionId = DeleteSessionBody(loginDao.getSession().sessionId)
        loginApi.deleteSession(sessionId)*/
        loginDao.deleteSession()
    }

}