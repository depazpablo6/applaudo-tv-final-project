package com.applaudostudios.data.login.model

import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.login.model.RequestToken
import com.google.gson.annotations.SerializedName

data class RequestTokenResponse(
        @field:SerializedName("success") val success: Boolean,
        @field:SerializedName("expires_at") val expiresAt: String,
        @field:SerializedName("request_token") val requestToken: String
) : DomainMapper<RequestToken> {
    override fun mapToDomainModel() = RequestToken(success, expiresAt, requestToken)
}