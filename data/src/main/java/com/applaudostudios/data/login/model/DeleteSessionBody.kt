package com.applaudostudios.data.login.model

import com.google.gson.annotations.SerializedName

data class DeleteSessionBody(
        @SerializedName("session_id")
        val sessionId: String
)