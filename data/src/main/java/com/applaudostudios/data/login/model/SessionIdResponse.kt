package com.applaudostudios.data.login.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.login.model.SessionId
import com.google.gson.annotations.SerializedName

@Entity
data class SessionIdResponse(
        @field:SerializedName("success") val success: Boolean,
        @field:SerializedName("session_id") @PrimaryKey val sessionId: String,
) : DomainMapper<SessionId> {
    override fun mapToDomainModel() = SessionId(success, sessionId)
}