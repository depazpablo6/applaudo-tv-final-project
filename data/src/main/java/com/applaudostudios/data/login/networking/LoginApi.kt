package com.applaudostudios.data.login.networking

import com.applaudostudios.data.login.model.DeleteSessionBody
import com.applaudostudios.data.login.model.RequestTokenResponse
import com.applaudostudios.data.login.model.SessionIdBody
import com.applaudostudios.data.login.model.SessionIdResponse
import retrofit2.http.*

interface LoginApi {

    @GET("authentication/token/new")
    suspend fun getRequestToken(@Query("api_key") key: String): RequestTokenResponse

    @Headers("content-type: application/json")
    @POST("authentication/session/new")
    suspend fun getSessionId(
            @Query("api_key") key: String,
            @Body request_token: SessionIdBody
    ): SessionIdResponse

    @HTTP(method = "DELETE", path = "/authentication/session", hasBody = true)
    suspend fun deleteSession(
            @Body sessionId: DeleteSessionBody
    )

}