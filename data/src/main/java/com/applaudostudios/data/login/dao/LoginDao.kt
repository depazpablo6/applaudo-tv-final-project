package com.applaudostudios.data.login.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.login.model.SessionIdResponse

@Dao
interface LoginDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertSession(sessionIdResponse: SessionIdResponse)

    @Query("DELETE FROM SessionIdResponse")
    suspend fun deleteSession()

    @Query("SELECT * FROM SessionIdResponse")
    suspend fun getSession(): SessionIdResponse

}