package com.applaudostudios.data.account.datasource

import android.util.Log
import androidx.paging.PagedList
import com.applaudostudios.data.BuildConfig
import com.applaudostudios.data.account.dao.FavoriteDao
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.domain.account.model.Favorite
import kotlinx.coroutines.runBlocking
import retrofit2.HttpException
import java.io.IOException

class FavoriteBoundaryCallback(
        private val accountApi: AccountApi,
        private val favoriteDao: FavoriteDao,
        private val sessionId: String
) : PagedList.BoundaryCallback<Favorite>() {

    private var currentPage = TvShowApi.FIRST_PAGE

    init {
        firstPopulate()
    }

    private fun firstPopulate() {
        runBlocking {
            try {
                val response = accountApi.getFavorites(BuildConfig.API_KEY, sessionId, currentPage)
                favoriteDao.fillFavorites(response.favoriteResponses)
            } catch (e: IOException) {
                //handle db exception
            } catch (e: HttpException) {
                //handle db exception
            }
        }
    }

    override fun onItemAtEndLoaded(itemAtEnd: Favorite) {
        runBlocking {
            try {
                val response = accountApi.getFavorites(BuildConfig.API_KEY, sessionId, currentPage.plus(1))
                currentPage = response.page
                favoriteDao.insertAll(response.favoriteResponses)
            } catch (e: IOException) {
                Log.i("ERROR DE BASE: ", "ERROR AL INSERTAR EN LA BASE")
            } catch (e: HttpException) {
                Log.i("ERROR DE NETWORK: ", "ERROR AL INSERTAR EN LA BASE")
            }
        }
    }

}