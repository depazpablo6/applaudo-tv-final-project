package com.applaudostudios.data.account.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.account.model.Favorite
import com.google.gson.annotations.SerializedName

@Entity
data class FavoriteResponse(
        @SerializedName("first_air_date")
        val firstAirDate: String,
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        @PrimaryKey
        val name: String,
        @SerializedName("overview")
        val overview: String,
        @SerializedName("poster_path")
        val posterPath: String,
        @SerializedName("vote_average")
        val voteAverage: Double,
) : DomainMapper<Favorite> {
    override fun mapToDomainModel() = Favorite(
            firstAirDate, id, name, overview, posterPath, voteAverage
    )
}