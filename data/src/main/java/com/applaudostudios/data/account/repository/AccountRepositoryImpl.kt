package com.applaudostudios.data.account.repository

import androidx.lifecycle.LiveData
import androidx.paging.LivePagedListBuilder
import androidx.paging.PagedList
import com.applaudostudios.data.account.dao.AccountDao
import com.applaudostudios.data.account.dao.FavoriteDao
import com.applaudostudios.data.account.datasource.FavoriteBoundaryCallback
import com.applaudostudios.data.account.networking.AccountApi
import com.applaudostudios.data.login.dao.LoginDao
import com.applaudostudios.data.tvshow.networking.TvShowApi
import com.applaudostudios.domain.account.model.Account
import com.applaudostudios.domain.account.model.Favorite
import com.applaudostudios.domain.account.repository.AccountRepository

class AccountRepositoryImpl(
        private val accountApi: AccountApi,
        private val accountDao: AccountDao,
        private val loginDao: LoginDao,
        private val favoriteDao: FavoriteDao
) : AccountRepository<LiveData<PagedList<Favorite>>> {
    override suspend fun getAccountInfo(): Account {
        return accountDao.getAccount().mapToDomainModel()
    }

    override suspend fun getFavorites(): LiveData<PagedList<Favorite>> {

        val config = PagedList.Config.Builder()
                .setPageSize(TvShowApi.NETWORK_PAGE)
                .setInitialLoadSizeHint(TvShowApi.NETWORK_PAGE)
                .setPrefetchDistance(TvShowApi.PRELOAD_DISTANCE)
                .setEnablePlaceholders(false)
                .build()

        val sessionId = loginDao.getSession().sessionId

        return LivePagedListBuilder(
                favoriteDao.getFavorites().mapByPage { list ->
                    list.map {
                        it.mapToDomainModel()
                    }
                },
                config
        ).setBoundaryCallback(FavoriteBoundaryCallback(accountApi, favoriteDao, sessionId))
                .build()


    }
}