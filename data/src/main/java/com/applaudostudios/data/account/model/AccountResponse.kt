package com.applaudostudios.data.account.model


import androidx.room.Entity
import androidx.room.PrimaryKey
import com.applaudostudios.data.common.mapper.DomainMapper
import com.applaudostudios.domain.account.model.Account
import com.google.gson.annotations.SerializedName

@Entity
data class AccountResponse(
        @SerializedName("id")
        @PrimaryKey
        val id: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("username")
        val username: String,
        val accountPath: String,
        val gravatarHash: String
) : DomainMapper<Account> {
    override fun mapToDomainModel() = Account(id, name, username, accountPath, gravatarHash)
}