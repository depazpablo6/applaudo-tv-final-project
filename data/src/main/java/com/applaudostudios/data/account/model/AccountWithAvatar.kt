package com.applaudostudios.data.account.model


import com.google.gson.annotations.SerializedName

data class AccountWithAvatar(
        @SerializedName("avatar")
        val avatar: Avatar,
        @SerializedName("id")
        val id: Int,
        @SerializedName("name")
        val name: String,
        @SerializedName("username")
        val username: String
)