package com.applaudostudios.data.account.dao

import androidx.paging.DataSource
import androidx.room.*
import com.applaudostudios.data.account.model.FavoriteResponse

@Dao
interface FavoriteDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAll(favorites: List<FavoriteResponse>)

    @Query("SELECT * FROM FavoriteResponse")
    fun getFavorites(): DataSource.Factory<Int, FavoriteResponse>

    @Query("DELETE FROM FavoriteResponse")
    suspend fun deleteAll()

    @Transaction
    suspend fun fillFavorites(favorites: List<FavoriteResponse>) {
        deleteAll()
        insertAll(favorites)
    }

}