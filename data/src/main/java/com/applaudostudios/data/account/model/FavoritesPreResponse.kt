package com.applaudostudios.data.account.model


import com.google.gson.annotations.SerializedName

data class FavoritesPreResponse(
        @SerializedName("page")
        val page: Int,
        @SerializedName("results")
        val favoriteResponses: List<FavoriteResponse>,
        @SerializedName("total_pages")
        val totalPages: Int,
        @SerializedName("total_results")
        val totalResults: Int
)