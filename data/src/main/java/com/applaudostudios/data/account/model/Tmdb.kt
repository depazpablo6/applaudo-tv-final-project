package com.applaudostudios.data.account.model


import com.google.gson.annotations.SerializedName

data class Tmdb(
        @SerializedName("avatar_path")
        val avatarPath: String
)