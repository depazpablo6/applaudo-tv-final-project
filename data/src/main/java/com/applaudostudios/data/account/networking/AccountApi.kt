package com.applaudostudios.data.account.networking

import com.applaudostudios.data.account.model.AccountWithAvatar
import com.applaudostudios.data.account.model.FavoritesPreResponse
import com.applaudostudios.data.tvshow.model.FavoriteBody
import retrofit2.http.*

interface AccountApi {

    @Headers("content-type: application/json;charset=utf-8")
    @POST("account/{account_id}/favorite")
    suspend fun markAsFavorite(
            @Path("account_id") accountId: Int,
            @Query("api_key") key: String,
            @Query("session_id") session: String,
            @Body bodyResponse: FavoriteBody,
    )

    @GET("account")
    suspend fun getAccountDetails(
            @Query("api_key") key: String,
            @Query("session_id") session: String
    ): AccountWithAvatar

    @GET("account/{account_id}/favorite/tv")
    suspend fun getFavorites(
            @Query("api_key") key: String,
            @Query("session_id") session: String,
            @Query("page") page: Int
    ): FavoritesPreResponse

}