package com.applaudostudios.data.account.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import com.applaudostudios.data.account.model.AccountResponse

@Dao
interface AccountDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    suspend fun insertAccount(account: AccountResponse)

    @Query("DELETE FROM AccountResponse")
    suspend fun deleteAccount()

    @Query("SELECT * FROM AccountResponse")
    suspend fun getAccount(): AccountResponse

}