package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.model.SessionId

interface GetSessionIdUseCase {
    suspend operator fun invoke(): SessionId?
}