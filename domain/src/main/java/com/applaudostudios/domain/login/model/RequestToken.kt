package com.applaudostudios.domain.login.model

data class RequestToken(
        val success: Boolean,
        val expiresAt: String,
        val requestToken: String
)