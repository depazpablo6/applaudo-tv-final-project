package com.applaudostudios.domain.login.model

data class SessionId(
        val success: Boolean,
        val sessionId: String
)