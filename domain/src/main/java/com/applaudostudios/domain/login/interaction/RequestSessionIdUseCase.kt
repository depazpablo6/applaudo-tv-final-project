package com.applaudostudios.domain.login.interaction

interface RequestSessionIdUseCase {
    suspend operator fun invoke(requestToken: String): Boolean
}