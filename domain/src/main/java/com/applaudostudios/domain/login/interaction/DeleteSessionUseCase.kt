package com.applaudostudios.domain.login.interaction

interface DeleteSessionUseCase {
    suspend operator fun invoke()
}