package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.repository.LoginRepository

class DeleteSessionUseCaseImpl(private val repository: LoginRepository) : DeleteSessionUseCase {
    override suspend fun invoke() = repository.deleteSession()
}