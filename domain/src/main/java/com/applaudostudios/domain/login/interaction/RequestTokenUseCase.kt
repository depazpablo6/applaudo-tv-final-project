package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.model.RequestToken

interface RequestTokenUseCase {
    suspend operator fun invoke(): RequestToken?
}