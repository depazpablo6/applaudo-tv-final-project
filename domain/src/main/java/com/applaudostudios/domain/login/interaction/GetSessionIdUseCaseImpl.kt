package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.repository.LoginRepository

class GetSessionIdUseCaseImpl(private val loginRepository: LoginRepository) : GetSessionIdUseCase {
    override suspend fun invoke() = loginRepository.getSessionId()
}