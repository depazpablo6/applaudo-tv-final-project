package com.applaudostudios.domain.login.repository

import com.applaudostudios.domain.login.model.RequestToken
import com.applaudostudios.domain.login.model.SessionId

interface LoginRepository {
    suspend fun requestToken(): RequestToken?
    suspend fun requestSessionId(requestToken: String): Boolean
    suspend fun getSessionId(): SessionId?
    suspend fun deleteSession()
}