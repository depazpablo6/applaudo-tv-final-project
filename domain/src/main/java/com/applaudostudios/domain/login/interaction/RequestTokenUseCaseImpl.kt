package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.repository.LoginRepository

class RequestTokenUseCaseImpl(private val loginRepository: LoginRepository) : RequestTokenUseCase {
    override suspend fun invoke() = loginRepository.requestToken()
}