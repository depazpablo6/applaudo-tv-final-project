package com.applaudostudios.domain.login.interaction

import com.applaudostudios.domain.login.repository.LoginRepository

class RequestSessionIdUseCaseImpl(private val loginRepository: LoginRepository) :
        RequestSessionIdUseCase {
    override suspend fun invoke(requestToken: String) =
            loginRepository.requestSessionId(requestToken)
}