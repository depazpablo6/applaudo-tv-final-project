package com.applaudostudios.domain.account.interaction

interface GetFavoritesUseCase<out T> {
    suspend operator fun invoke(): T
}