package com.applaudostudios.domain.account.interaction

import com.applaudostudios.domain.account.repository.AccountRepository

class GetAccountInfoUseCaseImpl<T : Any>(private val repository: AccountRepository<T>) : GetAccountInfoUseCase {
    override suspend fun invoke() = repository.getAccountInfo()
}