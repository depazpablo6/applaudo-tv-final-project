package com.applaudostudios.domain.account.repository

import com.applaudostudios.domain.account.model.Account

interface AccountRepository<out T> {

    suspend fun getAccountInfo(): Account
    suspend fun getFavorites(): T

}