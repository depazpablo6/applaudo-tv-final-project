package com.applaudostudios.domain.account.interaction

import com.applaudostudios.domain.account.repository.AccountRepository

class GetFavoritesUseCaseImpl<T : Any>(private val repository: AccountRepository<T>) : GetFavoritesUseCase<T> {
    override suspend fun invoke(): T = repository.getFavorites()
}