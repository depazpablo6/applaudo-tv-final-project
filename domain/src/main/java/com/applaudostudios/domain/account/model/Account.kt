package com.applaudostudios.domain.account.model

data class Account(
        val id: Int,
        val name: String,
        val username: String,
        val accountPath: String,
        val gravatarHash: String
)
