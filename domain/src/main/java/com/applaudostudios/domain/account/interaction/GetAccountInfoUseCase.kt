package com.applaudostudios.domain.account.interaction

import com.applaudostudios.domain.account.model.Account

interface GetAccountInfoUseCase {
    suspend operator fun invoke(): Account
}