package com.applaudostudios.domain.account.model

data class Favorite(
        val firstAirDate: String,
        val id: Int,
        val name: String,
        val overview: String,
        val posterPath: String,
        val voteAverage: Double,
)
