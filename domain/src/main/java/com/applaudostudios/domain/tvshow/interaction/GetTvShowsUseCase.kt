package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.util.TvShowFilter

interface GetTvShowsUseCase<out T> {
    suspend operator fun invoke(filter: TvShowFilter): T
}