package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository

class MarkAsFavoriteUseCaseImpl(private val repository: TvShowDetailsRepository) : MarkAsFavoriteUseCase {
    override suspend fun invoke(tvShowId: Int,
                                favorite: Boolean) = repository.markAsFavorite(tvShowId, favorite)
}