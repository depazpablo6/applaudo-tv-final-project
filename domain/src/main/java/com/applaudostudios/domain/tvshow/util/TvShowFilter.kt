package com.applaudostudios.domain.tvshow.util

enum class TvShowFilter {
    ON_AIR, AIRING_TODAY, POPULAR, TOP_RATED
}