package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.model.TvShowDetails

interface GetTvShowDetailsUseCase {
    suspend operator fun invoke(tvShowId: Int): TvShowDetails?
}