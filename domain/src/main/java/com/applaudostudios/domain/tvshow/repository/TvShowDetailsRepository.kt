package com.applaudostudios.domain.tvshow.repository

import com.applaudostudios.domain.tvshow.model.TvShowDetails

interface TvShowDetailsRepository {
    suspend fun getTvShowDetails(tvShowId: Int): TvShowDetails?
    suspend fun markAsFavorite(tvShowId: Int, favorite: Boolean)
    suspend fun isFavorite(tvShowId: Int): Boolean
}