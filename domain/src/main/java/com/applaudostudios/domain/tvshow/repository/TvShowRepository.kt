package com.applaudostudios.domain.tvshow.repository

import com.applaudostudios.domain.tvshow.util.TvShowFilter

interface TvShowRepository<out T> {
    suspend fun getTvShows(filter: TvShowFilter): T
}