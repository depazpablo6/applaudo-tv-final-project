package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.repository.TvShowRepository
import com.applaudostudios.domain.tvshow.util.TvShowFilter

class GetTvShowsUseCaseImpl<T : Any>(private val tvShowRepository: TvShowRepository<T>) :
        GetTvShowsUseCase<T> {
    override suspend fun invoke(filter: TvShowFilter): T = tvShowRepository.getTvShows(filter)

}