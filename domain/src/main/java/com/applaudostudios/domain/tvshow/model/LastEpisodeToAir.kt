package com.applaudostudios.domain.tvshow.model

data class LastEpisodeToAir(
        val id: Int,
        val seasonNumber: Int,
        val detailsId: Int
)
