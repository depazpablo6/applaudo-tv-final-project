package com.applaudostudios.domain.tvshow.model

data class TvShow(
        val firstAirDate: String,
        val id: Int,
        val name: String,
        val overview: String,
        val posterPath: String?,
        val voteAverage: Double,
)