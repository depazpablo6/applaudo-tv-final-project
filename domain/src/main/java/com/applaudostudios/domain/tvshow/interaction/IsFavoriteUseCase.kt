package com.applaudostudios.domain.tvshow.interaction

interface IsFavoriteUseCase {
    suspend operator fun invoke(tvShowId: Int): Boolean
}