package com.applaudostudios.domain.tvshow.model

import com.applaudostudios.domain.season.model.Season

data class TvShowDetails(
        val backdropPath: String?,
        val createdBy: List<CreatedBy>?,
        val firstAirDate: String?,
        val id: Int?,
        val lastEpisodeToAir: LastEpisodeToAir?,
        val name: String?,
        val numberOfSeasons: Int?,
        val overview: String?,
        val posterPath: String?,
        val seasons: List<Season>?,
        val status: String?,
        val voteAverage: Double?,
        var cast: List<Cast>?
)