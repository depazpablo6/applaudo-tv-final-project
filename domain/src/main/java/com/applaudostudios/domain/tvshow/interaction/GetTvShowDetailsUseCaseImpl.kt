package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository

class GetTvShowDetailsUseCaseImpl(private val tvShowDetailsRepository: TvShowDetailsRepository) :
        GetTvShowDetailsUseCase {
    override suspend fun invoke(tvShowId: Int) = tvShowDetailsRepository.getTvShowDetails(tvShowId)
}