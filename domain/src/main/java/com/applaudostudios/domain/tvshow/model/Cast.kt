package com.applaudostudios.domain.tvshow.model

data class Cast(
        val character: String,
        val id: Int,
        val name: String,
        val profilePath: String,
        val creditsId: Int
)