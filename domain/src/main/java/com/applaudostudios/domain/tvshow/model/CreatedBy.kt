package com.applaudostudios.domain.tvshow.model

data class CreatedBy(
        val id: Int,
        val name: String,
        val profilePath: String?,
        val detailsId: Int
)
