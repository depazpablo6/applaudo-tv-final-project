package com.applaudostudios.domain.tvshow.interaction

interface MarkAsFavoriteUseCase {
    suspend operator fun invoke(tvShowId: Int, favorite: Boolean)
}