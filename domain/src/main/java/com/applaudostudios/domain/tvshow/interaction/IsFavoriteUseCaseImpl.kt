package com.applaudostudios.domain.tvshow.interaction

import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository

class IsFavoriteUseCaseImpl(private val repository: TvShowDetailsRepository) : IsFavoriteUseCase {
    override suspend fun invoke(tvShowId: Int) = repository.isFavorite(tvShowId)
}