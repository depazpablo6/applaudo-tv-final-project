package com.applaudostudios.domain.season.model

data class SeasonWithEpisodes(
        val seasonNumber: Int,
        val episodes: List<Episode>
)