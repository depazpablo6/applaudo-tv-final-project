package com.applaudostudios.domain.season.interaction

import com.applaudostudios.domain.season.model.SeasonWithEpisodes

interface GetAllSeasonsUseCase {
    suspend operator fun invoke(tvShowId: Int): List<SeasonWithEpisodes>
}