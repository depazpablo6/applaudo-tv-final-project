package com.applaudostudios.domain.season.interaction

import com.applaudostudios.domain.season.repository.SeasonRepository

class GetAllSeasonsUseCaseImpl(private val repository: SeasonRepository) : GetAllSeasonsUseCase {
    override suspend fun invoke(tvShowId: Int) = repository.getAllSeasons(tvShowId)
}