package com.applaudostudios.domain.season.model

data class Season(
        val airDate: String,
        val id: Int,
        val name: String,
        val posterPath: String?,
        val seasonNumber: Int,
        val detailsId: Int
)
