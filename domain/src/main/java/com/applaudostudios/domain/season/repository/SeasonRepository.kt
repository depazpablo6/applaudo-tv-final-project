package com.applaudostudios.domain.season.repository

import com.applaudostudios.domain.season.model.SeasonWithEpisodes

interface SeasonRepository {
    suspend fun getAllSeasons(tvShowId: Int): List<SeasonWithEpisodes>
}