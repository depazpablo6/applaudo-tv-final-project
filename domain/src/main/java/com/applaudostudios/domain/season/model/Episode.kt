package com.applaudostudios.domain.season.model

data class Episode(
        val airDate: String,
        val episodeNumber: Int,
        val id: Int,
        val name: String,
        val overview: String,
        val stillPath: String,
        val voteAverage: Double,
        val seasonNumber: Int,
        val tvShowId: Int
)