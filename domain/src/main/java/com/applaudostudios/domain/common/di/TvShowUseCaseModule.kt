package com.applaudostudios.domain.common.di

import com.applaudostudios.domain.tvshow.interaction.*
import org.koin.dsl.module

val tvShowUseCase = module {

    factory<GetTvShowsUseCase<Any>> { GetTvShowsUseCaseImpl(tvShowRepository = get()) }
    factory<GetTvShowDetailsUseCase> { GetTvShowDetailsUseCaseImpl(tvShowDetailsRepository = get()) }
    factory<MarkAsFavoriteUseCase> { MarkAsFavoriteUseCaseImpl(repository = get()) }
    factory<IsFavoriteUseCase> { IsFavoriteUseCaseImpl(repository = get()) }

}