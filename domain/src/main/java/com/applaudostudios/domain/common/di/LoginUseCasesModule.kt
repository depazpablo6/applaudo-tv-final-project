package com.applaudostudios.domain.common.di

import com.applaudostudios.domain.login.interaction.*
import org.koin.dsl.module

val loginUseCasesModule = module {

    factory<RequestTokenUseCase> { RequestTokenUseCaseImpl(loginRepository = get()) }
    factory<RequestSessionIdUseCase> { RequestSessionIdUseCaseImpl(loginRepository = get()) }
    factory<GetSessionIdUseCase> { GetSessionIdUseCaseImpl(loginRepository = get()) }
    factory<DeleteSessionUseCase> { DeleteSessionUseCaseImpl(repository = get()) }

}