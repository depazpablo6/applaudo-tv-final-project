package com.applaudostudios.domain.common.di

import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCase
import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCaseImpl
import org.koin.dsl.module

val seasonModule = module {

    factory<GetAllSeasonsUseCase> { GetAllSeasonsUseCaseImpl(repository = get()) }

}