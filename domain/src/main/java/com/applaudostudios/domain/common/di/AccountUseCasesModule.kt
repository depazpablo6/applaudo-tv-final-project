package com.applaudostudios.domain.common.di

import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCase
import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCaseImpl
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCase
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCaseImpl
import com.applaudostudios.domain.account.model.Account
import org.koin.dsl.module

val accountModule = module {
    factory<GetFavoritesUseCase<Any>> { GetFavoritesUseCaseImpl(repository = get()) }
    factory<GetAccountInfoUseCase> { GetAccountInfoUseCaseImpl<Account>(repository = get()) }
}