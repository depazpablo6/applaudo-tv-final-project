package com.applaudostudios.domain.account

import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCase
import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCaseImpl
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCase
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCaseImpl
import com.applaudostudios.domain.account.model.Account
import com.applaudostudios.domain.account.model.Favorite
import com.applaudostudios.domain.account.repository.AccountRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class GetAccountInfoUseCaseTest {

    private val accountRepository: AccountRepository<Any> = mock(AccountRepository::class.java) as AccountRepository<Any>
    private lateinit var getAccountInfoUseCase: GetAccountInfoUseCase
    private lateinit var getFavoritesUseCase: GetFavoritesUseCase<Any?>

    private val testAccount: Account = Account(
            1,
            "Pablo De Paz",
            "revenge98",
            "https://profile.image.com",
            "https://profile.gravatar.com"
    )

    private val favorite1 = Favorite("10-11-1998", 1, "Show 1", "Show 1", "https://showimage.com", 10.0)
    private val favorite2 = Favorite("04-04-1998", 2, "Show 2", "Show 2", "https://showimage.com", 10.0)
    private val favorite3 = Favorite("09-02-2007", 3, "Show 3", "Show 3", "https://showimage.com", 10.0)
    private val favoriteList = listOf(favorite1, favorite2, favorite3)

    @Before
    fun mockBehavior() {
        getFavoritesUseCase = GetFavoritesUseCaseImpl(accountRepository)
        getAccountInfoUseCase = GetAccountInfoUseCaseImpl(accountRepository)

        runBlocking {
            `when`(accountRepository.getAccountInfo()).thenReturn(testAccount)
            `when`(accountRepository.getFavorites()).thenReturn(favoriteList)
        }
    }

    @Test
    fun getAccountInfo_completed_returnsAccountObject() {
        runBlocking {
            val response = getAccountInfoUseCase()
            assertThat(response, IsEqual(testAccount))
        }
    }

    @Test
    fun getFavorites_completed_returnsFavoriteList() {
        runBlocking {
            val response = getFavoritesUseCase()
            assertThat(response, IsEqual(favoriteList))
        }
    }

}