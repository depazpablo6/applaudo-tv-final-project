package com.applaudostudios.domain.season

import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCase
import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCaseImpl
import com.applaudostudios.domain.season.model.Episode
import com.applaudostudios.domain.season.model.Season
import com.applaudostudios.domain.season.model.SeasonWithEpisodes
import com.applaudostudios.domain.season.repository.SeasonRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class GetAllSeasonsUseCaseTest {

    private val seasonRepository = mock(SeasonRepository::class.java)
    private lateinit var getAllSeasonUseCase: GetAllSeasonsUseCase
    private val tvShowId = 1

    private val season1 = Season(
            "airDate1",
            1,
            "Season 1",
            "https://posterimage.com",
            1,
            tvShowId
    )

    private val season2 = Season(
            "airDate2",
            2,
            "Season 2",
            "https://posterimage.com",
            2,
            tvShowId
    )

    private val episode1 = Episode("airDate1",
            1,
            1,
            "name 1",
            "overview 1",
            "https://episodeimage.com",
            10.0,
            season1.seasonNumber,
            tvShowId)
    private val episode2 = Episode("airDate2",
            2,
            2,
            "name 2",
            "overview 2",
            "https://episodeimage.com",
            10.0,
            season1.seasonNumber,
            tvShowId)
    private val episode3 = Episode("airDate3",
            3,
            3,
            "name 3",
            "overview 3",
            "https://episodeimage.com",
            10.0,
            season1.seasonNumber,
            tvShowId)
    private val episodes1 = listOf(episode1, episode2, episode3)
    private val seasonWithEpisodes1 = SeasonWithEpisodes(1, episodes1)

    private val episode4 = Episode("airDate4",
            1,
            4,
            "name 4",
            "overview 4",
            "https://episodeimage.com",
            10.0,
            season2.seasonNumber,
            tvShowId)
    private val episode5 = Episode("airDate5",
            5,
            5,
            "name 5",
            "overview 5",
            "https://episodeimage.com",
            10.0,
            season2.seasonNumber,
            tvShowId)
    private val episode6 = Episode("airDate6",
            6,
            6,
            "name 6",
            "overview 6",
            "https://episodeimage.com",
            10.0,
            season2.seasonNumber,
            tvShowId)
    private val episodes2 = listOf(episode4, episode5, episode6)
    private val seasonWithEpisodes2 = SeasonWithEpisodes(1, episodes2)

    private val seasons = listOf(seasonWithEpisodes1, seasonWithEpisodes2)

    @Before
    fun mockBehavior() {
        getAllSeasonUseCase = GetAllSeasonsUseCaseImpl(seasonRepository)
        runBlocking {
            `when`(seasonRepository.getAllSeasons(tvShowId)).thenReturn(seasons)
        }
    }

    @Test
    fun getAllSeasonUseCase_completed_returnListSeasonsWithEpisodes() {
        runBlocking {
            val response = getAllSeasonUseCase(tvShowId)
            assertThat(response, IsEqual(seasons))
        }
    }

}