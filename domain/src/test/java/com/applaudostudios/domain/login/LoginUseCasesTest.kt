package com.applaudostudios.domain.login

import com.applaudostudios.domain.login.interaction.*
import com.applaudostudios.domain.login.model.RequestToken
import com.applaudostudios.domain.login.model.SessionId
import com.applaudostudios.domain.login.repository.LoginRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class LoginUseCasesTest {

    private val loginRepository = mock(LoginRepository::class.java)
    private lateinit var deleteSessionUseCase: DeleteSessionUseCase
    private lateinit var getSessionIdUseCase: GetSessionIdUseCase
    private lateinit var requestSessionIdUseCase: RequestSessionIdUseCase
    private lateinit var requestTokenUseCase: RequestTokenUseCase

    private val session = SessionId(true, "sessionId")
    private val sessionDatabase = mutableListOf(session)
    private val requestToken = "requestToken"
    private val requestTokenObject = RequestToken(true, "tomorrow", requestToken)


    @Before
    fun mockBehavior() {

        deleteSessionUseCase = DeleteSessionUseCaseImpl(loginRepository)
        getSessionIdUseCase = GetSessionIdUseCaseImpl(loginRepository)
        requestSessionIdUseCase = RequestSessionIdUseCaseImpl(loginRepository)
        requestTokenUseCase = RequestTokenUseCaseImpl(loginRepository)

        runBlocking {
            `when`(loginRepository.deleteSession()).then { sessionDatabase.clear() }
            `when`(loginRepository.getSessionId()).thenReturn(session)
            `when`(loginRepository.requestSessionId(requestTokenObject.requestToken)).thenReturn(true)
            `when`(loginRepository.requestToken()).thenReturn(requestTokenObject)
        }
    }

    @Test
    fun deleteSession_completed_deletesSessionOfDatabase() {
        runBlocking {
            loginRepository.deleteSession()
            assert(sessionDatabase.isEmpty())
        }
    }

    @Test
    fun getSessionId_completed_returnSession() {
        runBlocking {
            val response = loginRepository.getSessionId()
            assertThat(response, IsEqual(session))
        }
    }

    @Test
    fun requestSessionId_completed_returnTrue() {
        runBlocking {
            val response = loginRepository.requestSessionId(requestTokenObject.requestToken)
            assertThat(response, IsEqual(true))
        }
    }

    @Test
    fun requestToken_completed_returnRequestToken() {
        runBlocking {
            val response = loginRepository.requestToken()
            assertThat(response, IsEqual(requestTokenObject))
        }
    }

}