package com.applaudostudios.domain.tvshow

import com.applaudostudios.domain.tvshow.interaction.GetTvShowsUseCase
import com.applaudostudios.domain.tvshow.interaction.GetTvShowsUseCaseImpl
import com.applaudostudios.domain.tvshow.model.TvShow
import com.applaudostudios.domain.tvshow.repository.TvShowRepository
import com.applaudostudios.domain.tvshow.util.TvShowFilter
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class TvShowUseCaseTest {

    private val tvShowRepository: TvShowRepository<Any> = mock(TvShowRepository::class.java) as TvShowRepository<Any>
    private lateinit var getTvShowsUseCaseTest: GetTvShowsUseCase<Any>

    private val tvShow1 = TvShow("10-11-1998", 1, "Show 1", "Show 1", "https://showimage.com", 10.0)
    private val tvShow2 = TvShow("04-04-1998", 2, "Show 2", "Show 2", "https://showimage.com", 10.0)
    private val tvShow3 = TvShow("09-02-2007", 3, "Show 3", "Show 3", "https://showimage.com", 10.0)
    private val tvShowList = listOf(tvShow1, tvShow2, tvShow3)

    @Before
    fun mockBehavior() {
        getTvShowsUseCaseTest = GetTvShowsUseCaseImpl(tvShowRepository)
        runBlocking {
            `when`(tvShowRepository.getTvShows(TvShowFilter.POPULAR)).thenReturn(tvShowList)
        }
    }

    @Test
    fun getTvShowsUseCase_completed_returnsTvShowList() {
        runBlocking {
            val response = getTvShowsUseCaseTest(TvShowFilter.POPULAR)
            assertThat(response, IsEqual(tvShowList))
        }
    }

}