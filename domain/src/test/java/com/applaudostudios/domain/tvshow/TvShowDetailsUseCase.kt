package com.applaudostudios.domain.tvshow

import com.applaudostudios.domain.season.model.Season
import com.applaudostudios.domain.tvshow.interaction.*
import com.applaudostudios.domain.tvshow.model.Cast
import com.applaudostudios.domain.tvshow.model.CreatedBy
import com.applaudostudios.domain.tvshow.model.LastEpisodeToAir
import com.applaudostudios.domain.tvshow.model.TvShowDetails
import com.applaudostudios.domain.tvshow.repository.TvShowDetailsRepository
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class TvShowDetailsUseCase {

    private val tvShowDetailsRepository = mock(TvShowDetailsRepository::class.java)
    private lateinit var getTvShowDetailsUseCase: GetTvShowDetailsUseCase
    private lateinit var isFavoriteUseCase: IsFavoriteUseCase
    private lateinit var markAsFavoriteUseCase: MarkAsFavoriteUseCase

    private val tvShowId = 1
    private var favorite = false

    private val createdBy1 = CreatedBy(1, "Creator 1", "https://image.com", tvShowId)
    private val createdBy2 = CreatedBy(2, "Creator 2", "https://image.com", tvShowId)
    private val listCreator = listOf(createdBy1, createdBy2)
    private val lastEpisodeToAir = LastEpisodeToAir(1, 1, tvShowId)
    private val season1 = Season(
            "airDate",
            1,
            "Season 1",
            "https://image.com",
            1,
            tvShowId,
    )
    private val seasons = listOf(season1)
    private val cast1 = Cast("Char 1", 1, "Name 1", "https://image.com", tvShowId)
    private val cast2 = Cast("Char 2", 1, "Name 2", "https://image.com", tvShowId)
    private val cast = listOf(cast1, cast2)


    private val tvShowDetails = TvShowDetails(
            "https://image.com",
            listCreator,
            "airDate",
            1,
            lastEpisodeToAir,
            "Name",
            1,
            "Overview 1",
            "https://image.com",
            seasons,
            "Ended",
            10.0,
            cast
    )

    @Before
    fun mockBehavior() {
        getTvShowDetailsUseCase = GetTvShowDetailsUseCaseImpl(tvShowDetailsRepository)
        isFavoriteUseCase = IsFavoriteUseCaseImpl(tvShowDetailsRepository)
        markAsFavoriteUseCase = MarkAsFavoriteUseCaseImpl(tvShowDetailsRepository)

        runBlocking {
            `when`(tvShowDetailsRepository.isFavorite(tvShowId)).thenReturn(true)
            `when`(tvShowDetailsRepository.markAsFavorite(tvShowId, true)).then { kotlin.run { favorite = true } }
            `when`(tvShowDetailsRepository.getTvShowDetails(tvShowId)).thenReturn(tvShowDetails)
        }
    }

    @Test
    fun getTvShowDetailsUseCase_completed_returnsTvShowDetails() {
        runBlocking {
            val response = getTvShowDetailsUseCase(tvShowId)
            assertThat(response, IsEqual(tvShowDetails))
        }
    }

    @Test
    fun markAsFavoriteUseCase_completed_changeFavoriteBoolean() {
        runBlocking {
            markAsFavoriteUseCase(tvShowId, true)
            assertThat(favorite, IsEqual(true))
        }
    }

    @Test
    fun isFavorite_completed_returnsBoolean() {
        runBlocking {
            val response = isFavoriteUseCase(tvShowId)
            assertThat(response, IsEqual(true))
        }
    }

}