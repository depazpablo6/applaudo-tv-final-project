package com.applaudostudios.applaudotv.account.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCase
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCase
import com.applaudostudios.domain.account.model.Account
import com.applaudostudios.domain.account.model.Favorite
import kotlinx.coroutines.launch

class AccountViewModel(
        private val getAccountInfoUseCase: GetAccountInfoUseCase,
        private val getFavoritesUseCase: GetFavoritesUseCase<LiveData<PagedList<Favorite>>>
) : ViewModel() {

    val accountInfo = MutableLiveData<Account>()
    private lateinit var favoriteShows: LiveData<PagedList<Favorite>>
    val favoriteShowsMutable = MutableLiveData<PagedList<Favorite>>()
    val mediator = MediatorLiveData<Unit>()

    init {
        getAccountInfo()
        getFavoriteShows()
    }

    private fun getAccountInfo() {
        viewModelScope.launch {
            accountInfo.value = getAccountInfoUseCase()
        }
    }

    fun getFavoriteShows() {
        viewModelScope.launch {
            favoriteShows = getFavoritesUseCase()
            mediator.addSource(favoriteShows) { favoriteShowsMutable.value = it }
        }
    }

}