package com.applaudostudios.applaudotv.account.ui

import android.content.Intent
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.account.adapter.FavoriteAdapter
import com.applaudostudios.applaudotv.account.viewmodel.AccountViewModel
import com.applaudostudios.applaudotv.common.util.loadCircleImage
import com.applaudostudios.applaudotv.databinding.ActivityAccountBinding
import com.applaudostudios.applaudotv.tvshow.ui.TvShowDetailsActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

class AccountActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAccountBinding
    private val accountViewModel: AccountViewModel by viewModel()
    private lateinit var adapter: FavoriteAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAccountBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        initAdapter()

        accountViewModel.mediator.observe(this, {})

        accountViewModel.accountInfo.observe(this, {
            binding.accountUsername.text = it.username
            setImage(it.accountPath)
        })

        accountViewModel.favoriteShowsMutable.observe(this, {
            if (it.isEmpty())
                Toast.makeText(applicationContext,
                        applicationContext.getString(R.string.favoriteEmpty),
                        Toast.LENGTH_LONG)
                        .show()
            else adapter.submitList(it)
            binding.refreshList.isRefreshing = false
        })

        binding.refreshList.setOnRefreshListener {
            accountViewModel.getFavoriteShows()
        }

    }

    override fun onResume() {
        super.onResume()
        accountViewModel.getFavoriteShows()
    }

    private fun initAdapter() {
        adapter = FavoriteAdapter {
            val intent = Intent(this, TvShowDetailsActivity::class.java).apply {
                putExtra(TvShowDetailsActivity.TV_SHOW_ID, it)
            }
            startActivity(intent)
        }

        binding.favoriteShowsRecycler.adapter = adapter
    }

    private fun setImage(url: String) {
        url.loadCircleImage(binding.accountImage)
    }

}