package com.applaudostudios.applaudotv.account.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.common.util.loadImage
import com.applaudostudios.domain.account.model.Favorite

class FavoriteAdapter(private val onClickItem: (Int) -> Unit) :
        PagedListAdapter<Favorite, FavoriteAdapter.FavoriteViewHolder>(FavoriteComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FavoriteViewHolder {
        return FavoriteViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: FavoriteViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, onClickItem) }
    }

    class FavoriteViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val tvShowAirDate: TextView = itemView.findViewById(R.id.tvShowAirDate)
        private val tvShowVoteAverage: TextView = itemView.findViewById(R.id.tvShowAverage)
        private val tvShowTitle: TextView = itemView.findViewById(R.id.tvShowTitle)
        private val tvShowDescription: TextView = itemView.findViewById(R.id.tvShowDescription)
        private val tvShowImage: ImageView = itemView.findViewById(R.id.tvShowImage)


        fun bind(favorite: Favorite, clickItem: (Int) -> Unit) {

            tvShowAirDate.text = favorite.firstAirDate
            tvShowVoteAverage.text = favorite.voteAverage.toString()
            tvShowTitle.text = favorite.name
            tvShowDescription.text = favorite.overview

            favorite.posterPath.let {
                it.loadImage(tvShowImage)
            }

            itemView.setOnClickListener {
                clickItem(favorite.id)
            }
        }

        companion object {
            fun create(parent: ViewGroup): FavoriteViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.tv_show_card_favorite, parent, false)
                return FavoriteViewHolder(view)
            }
        }
    }

    class FavoriteComparator : DiffUtil.ItemCallback<Favorite>() {
        override fun areItemsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Favorite, newItem: Favorite): Boolean {
            return oldItem == newItem
        }

    }

}