package com.applaudostudios.applaudotv.common.util

import android.widget.ImageView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.tvshow.util.IMAGE_BASE_URL
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions

fun String.loadImage(view: ImageView) {
    Glide.with(view.rootView)
            .load(IMAGE_BASE_URL.plus(this))
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.image_error)
            .centerCrop()
            .into(view)
}

fun String.loadCircleImage(view: ImageView) {
    Glide.with(view.rootView)
            .load(IMAGE_BASE_URL.plus(this))
            .diskCacheStrategy(DiskCacheStrategy.RESOURCE)
            .transition(DrawableTransitionOptions.withCrossFade())
            .error(R.drawable.image_error)
            .circleCrop()
            .into(view)
}