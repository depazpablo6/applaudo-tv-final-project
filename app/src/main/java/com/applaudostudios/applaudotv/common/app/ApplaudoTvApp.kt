package com.applaudostudios.applaudotv.common.app

import android.app.Application
import com.applaudostudios.applaudotv.common.di.viewModelModule
import com.applaudostudios.data.common.di.databaseModule
import com.applaudostudios.data.common.di.networkModule
import com.applaudostudios.data.common.di.repositoryModule
import com.applaudostudios.domain.common.di.accountModule
import com.applaudostudios.domain.common.di.loginUseCasesModule
import com.applaudostudios.domain.common.di.seasonModule
import com.applaudostudios.domain.common.di.tvShowUseCase
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin

class ApplaudoTvApp : Application() {
    override fun onCreate() {
        super.onCreate()
        startKoin {
            androidContext(this@ApplaudoTvApp)
            modules(appModules + domainModules + dataModules)
        }
    }
}

val appModules = listOf(viewModelModule)
val domainModules = listOf(loginUseCasesModule, tvShowUseCase, seasonModule, accountModule)
val dataModules = listOf(networkModule, repositoryModule, databaseModule)
