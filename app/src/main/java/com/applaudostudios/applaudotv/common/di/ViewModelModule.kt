package com.applaudostudios.applaudotv.common.di

import com.applaudostudios.applaudotv.account.viewmodel.AccountViewModel
import com.applaudostudios.applaudotv.login.viewmodel.MainViewModel
import com.applaudostudios.applaudotv.season.viewmodel.SeasonViewModel
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowDetailsViewModel
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowsViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module {
    viewModel {
        MainViewModel(
                requestTokenUseCase = get(),
                requestSessionIdUseCase = get(),
                getSessionIdUseCase = get()
        )
    }
    viewModel { TvShowsViewModel(getTvShowsUseCase = get(), deleteSessionUseCase = get()) }
    viewModel {
        TvShowDetailsViewModel(
                getTvShowDetailsUseCase = get(),
                markAsFavoriteUseCase = get(),
                isFavoriteUseCase = get()
        )
    }
    viewModel { SeasonViewModel(getAllSeasonsUseCase = get()) }
    viewModel {
        AccountViewModel(
                getAccountInfoUseCase = get(),
                getFavoritesUseCase = get()
        )
    }
}
