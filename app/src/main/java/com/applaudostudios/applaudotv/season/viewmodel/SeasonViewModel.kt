package com.applaudostudios.applaudotv.season.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCase
import com.applaudostudios.domain.season.model.SeasonWithEpisodes
import kotlinx.coroutines.launch

class SeasonViewModel(
        private val getAllSeasonsUseCase: GetAllSeasonsUseCase,
) : ViewModel() {

    val allSeasons = MutableLiveData<List<SeasonWithEpisodes>>()

    fun getAllSeasons(tvShowId: Int) {
        viewModelScope.launch {
            allSeasons.value = getAllSeasonsUseCase(tvShowId)
        }
    }

}