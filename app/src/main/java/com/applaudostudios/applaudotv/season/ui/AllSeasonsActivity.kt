package com.applaudostudios.applaudotv.season.ui

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.applaudotv.databinding.ActivityAllSeasonsBinding
import com.applaudostudios.applaudotv.season.adapter.SeasonAdapter
import com.applaudostudios.applaudotv.season.viewmodel.SeasonViewModel
import com.applaudostudios.applaudotv.tvshow.ui.TvShowDetailsActivity.Companion.TV_SHOW_ID
import org.koin.androidx.viewmodel.ext.android.viewModel


class AllSeasonsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAllSeasonsBinding
    private val seasonsViewModel: SeasonViewModel by viewModel()
    private lateinit var seasonAdapter: SeasonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAllSeasonsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val tvShowId = intent.getIntExtra(TV_SHOW_ID, 0)

        val seasonRecycler = binding.seasonsRecycler
        seasonRecycler.layoutManager = LinearLayoutManager(this)

        seasonsViewModel.getAllSeasons(tvShowId)

        seasonsViewModel.allSeasons.observe(this, {
            seasonAdapter = SeasonAdapter(applicationContext, it)
            seasonRecycler.adapter = seasonAdapter
            seasonAdapter.submitList(it)
            binding.pBar.visibility = View.INVISIBLE
        })

    }
}