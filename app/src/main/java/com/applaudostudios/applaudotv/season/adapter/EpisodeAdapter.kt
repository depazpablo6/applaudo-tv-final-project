package com.applaudostudios.applaudotv.season.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.common.util.loadImage
import com.applaudostudios.domain.season.model.Episode

class EpisodeAdapter(private val context: Context) : ListAdapter<Episode, EpisodeAdapter.EpisodeViewHolder>(EpisodeAdapter.EpisodeComparator()) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): EpisodeViewHolder {
        return EpisodeViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: EpisodeViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class EpisodeViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val episodeImage: ImageView = itemView.findViewById(R.id.episodeImage)
        private val episodeName: TextView = itemView.findViewById(R.id.episodeName)
        private val episodeNumber: TextView = itemView.findViewById(R.id.episodeNumber)
        private val episodeDescription: TextView = itemView.findViewById(R.id.episodeDescription)
        private val episodeAverage: TextView = itemView.findViewById(R.id.episodeAverage)

        fun bind(episode: Episode) {
            episodeName.text = episode.name
            episodeNumber.text = episode.episodeNumber.toString().plus(".")
            episodeAverage.text = episode.voteAverage.toString()
            episodeDescription.text = episode.overview
            episode.stillPath.loadImage(episodeImage)
        }

        companion object {
            fun create(parent: ViewGroup): EpisodeViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.episode_card, parent, false)
                return EpisodeViewHolder(view)
            }
        }
    }

    class EpisodeComparator : DiffUtil.ItemCallback<Episode>() {
        override fun areItemsTheSame(oldItem: Episode, newItem: Episode): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Episode, newItem: Episode): Boolean {
            return oldItem == newItem
        }

    }

}