package com.applaudostudios.applaudotv.season.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.domain.season.model.SeasonWithEpisodes


class SeasonAdapter(val context: Context, private val episodes: List<SeasonWithEpisodes>) : ListAdapter<SeasonWithEpisodes, SeasonAdapter.SeasonViewHolder>(SeasonAdapter.SeasonComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SeasonViewHolder {
        return SeasonViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: SeasonViewHolder, position: Int) {
        if (getItem(position).episodes.isEmpty()) {
            holder.seasonNumber.text = context.getString(R.string.seasonName)
                    .plus(getItem(position).seasonNumber.toString())
                    .plus(": ")
                    .plus(context.getString(R.string.noDataLoaded))
        } else {
            holder.seasonNumber.text = context.getString(R.string.seasonName).plus(getItem(position).seasonNumber.toString())
        }
        val episodeAdapter = EpisodeAdapter(context)
        holder.episodesRecyclerView.adapter = episodeAdapter
        holder.episodesRecyclerView.layoutManager = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        episodeAdapter.submitList(getItem(position).episodes)
    }

    class SeasonViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val seasonNumber: TextView = itemView.findViewById(R.id.seasonName)
        val episodesRecyclerView: RecyclerView = itemView.findViewById(R.id.episodesRecycler)

        companion object {
            fun create(parent: ViewGroup): SeasonViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.season_item, parent, false)
                return SeasonViewHolder(view)
            }
        }
    }

    class SeasonComparator : DiffUtil.ItemCallback<SeasonWithEpisodes>() {
        override fun areItemsTheSame(oldItem: SeasonWithEpisodes, newItem: SeasonWithEpisodes): Boolean {
            return oldItem.seasonNumber == newItem.seasonNumber
        }

        override fun areContentsTheSame(oldItem: SeasonWithEpisodes, newItem: SeasonWithEpisodes): Boolean {
            return oldItem == newItem
        }

    }

}