package com.applaudostudios.applaudotv.login.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.databinding.ActivityMainBinding
import com.applaudostudios.applaudotv.login.ui.AuthenticationActivity.Companion.ERROR_CODE
import com.applaudostudios.applaudotv.login.ui.AuthenticationActivity.Companion.REQUEST_TOKEN
import com.applaudostudios.applaudotv.login.ui.AuthenticationActivity.Companion.SUCCESS_CODE
import com.applaudostudios.applaudotv.login.viewmodel.MainViewModel
import com.applaudostudios.applaudotv.tvshow.ui.TvShowsActivity
import org.koin.androidx.viewmodel.ext.android.viewModel

@Suppress("DEPRECATION")
class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val mainViewModel: MainViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        setOnClickListener()
        liveDataModifyActions()
    }

    private fun liveDataModifyActions() {
        mainViewModel.requestToken.observe(this, {
            it?.let {
                val intent = Intent(applicationContext, AuthenticationActivity::class.java).apply {
                    putExtra(REQUEST_TOKEN, mainViewModel.requestToken.value?.requestToken)
                }
                startActivityForResult(intent, SUCCESS_CODE)
            } ?: run {
                binding.pBar.visibility = View.INVISIBLE
                binding.loginButton.visibility = View.VISIBLE
                Toast.makeText(applicationContext, getString(R.string.noRequestToken), Toast.LENGTH_LONG).show()
            }
        })
        mainViewModel.sessionId.observe(this, {
            val intent = Intent(applicationContext, TvShowsActivity::class.java)
            startActivity(intent)
            finish()
        })
        mainViewModel.alreadyLogedIn.observe(this, {
            if (mainViewModel.alreadyLogedIn.value != null) {
                binding.pBar.visibility = View.VISIBLE
                binding.loginButton.visibility = View.INVISIBLE
                val intent = Intent(applicationContext, TvShowsActivity::class.java)
                startActivity(intent)
                finish()
            }
        })
    }

    private fun setOnClickListener() {
        binding.loginButton.setOnClickListener {
            binding.pBar.visibility = View.VISIBLE
            binding.loginButton.visibility = View.INVISIBLE
            mainViewModel.getRequestToken()
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == SUCCESS_CODE && resultCode != ERROR_CODE) mainViewModel.requestSessionId()
        else {
            binding.pBar.visibility = View.INVISIBLE
            binding.loginButton.visibility = View.VISIBLE
            Toast.makeText(applicationContext, getString(R.string.sessionError), Toast.LENGTH_LONG).show()
        }
    }

}