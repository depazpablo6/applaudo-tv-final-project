package com.applaudostudios.applaudotv.login.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudostudios.domain.login.interaction.GetSessionIdUseCase
import com.applaudostudios.domain.login.interaction.RequestSessionIdUseCase
import com.applaudostudios.domain.login.interaction.RequestTokenUseCase
import com.applaudostudios.domain.login.model.RequestToken
import com.applaudostudios.domain.login.model.SessionId
import kotlinx.coroutines.launch

class MainViewModel(
        private val requestTokenUseCase: RequestTokenUseCase,
        private val requestSessionIdUseCase: RequestSessionIdUseCase,
        private val getSessionIdUseCase: GetSessionIdUseCase
) : ViewModel() {

    val requestToken = MutableLiveData<RequestToken?>()
    val sessionId = MutableLiveData<SessionId>()
    val alreadyLogedIn = MutableLiveData<SessionId>()

    init {
        viewModelScope.launch { alreadyLogedIn.value = getSessionIdUseCase() }
    }

    fun getRequestToken() = viewModelScope.launch {
        requestToken.value = requestTokenUseCase()
    }

    private fun getSessionId() = viewModelScope.launch {
        sessionId.value = getSessionIdUseCase()
    }

    fun requestSessionId() = viewModelScope.launch {
        requestToken.value?.let { requestSessionIdUseCase(it.requestToken) }
        getSessionId()
    }

}