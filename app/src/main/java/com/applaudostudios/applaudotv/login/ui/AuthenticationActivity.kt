package com.applaudostudios.applaudotv.login.ui

import android.annotation.SuppressLint
import android.os.Bundle
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.appcompat.app.AppCompatActivity
import com.applaudostudios.applaudotv.databinding.ActivityAuthenticationBinding

class AuthenticationActivity : AppCompatActivity() {

    private lateinit var binding: ActivityAuthenticationBinding

    @SuppressLint("SetJavaScriptEnabled")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityAuthenticationBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val token = intent.getStringExtra(REQUEST_TOKEN)

        binding.loginWebView.loadUrl(AUTHENTICATION_URL.plus(token))
        binding.loginWebView.settings.javaScriptEnabled = true
        binding.loginWebView.webViewClient = object : WebViewClient() {
            override fun shouldOverrideUrlLoading(
                    view: WebView?,
                    request: WebResourceRequest?
            ): Boolean {
                view?.loadUrl(request?.url.toString())
                return false
            }

            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                url?.let {
                    if (url.contains(AUTHENTICATION_SUCCESS)) {
                        setResult(SUCCESS_CODE)
                        finish()
                    }
                }
            }

        }

    }

    override fun onBackPressed() {
        setResult(ERROR_CODE)
        super.onBackPressed()
    }

    companion object {
        const val REQUEST_TOKEN = "REQUEST_TOKEN"
        private const val AUTHENTICATION_URL = "https://www.themoviedb.org/authenticate/"
        private const val AUTHENTICATION_SUCCESS = "/allow"
        const val SUCCESS_CODE = 200
        const val ERROR_CODE = 400
    }

}