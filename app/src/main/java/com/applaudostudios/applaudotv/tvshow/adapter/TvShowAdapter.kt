package com.applaudostudios.applaudotv.tvshow.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.paging.PagedListAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.common.util.loadImage
import com.applaudostudios.domain.tvshow.model.TvShow

class TvShowAdapter(private val onClickItem: (Int) -> Unit) :
        PagedListAdapter<TvShow, TvShowAdapter.TvShowViewHolder>(TvShowComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TvShowViewHolder {
        return TvShowViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: TvShowViewHolder, position: Int) {
        getItem(position)?.let { holder.bind(it, onClickItem) }
        val airDateText = holder.itemView.context.getString(R.string.airDate)
                .plus(" ")
                .plus(getItem(position)?.firstAirDate)
        holder.tvShowAirDate.text = airDateText
    }

    class TvShowViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val tvShowAirDate: TextView = itemView.findViewById(R.id.tvShowAirDate)
        private val tvShowVoteAverage: TextView = itemView.findViewById(R.id.tvShowAverage)
        private val tvShowTitle: TextView = itemView.findViewById(R.id.tvShowTitle)
        private val tvShowDescription: TextView = itemView.findViewById(R.id.tvShowDescription)
        private val tvShowImage: ImageView = itemView.findViewById(R.id.tvShowImage)

        fun bind(tvShow: TvShow, clickItem: (Int) -> Unit) {

            val airDateText = "Air Date: " + tvShow.firstAirDate
            tvShowAirDate.text = airDateText
            tvShowDescription.text = tvShow.overview
            tvShowTitle.text = tvShow.name
            tvShowVoteAverage.text = tvShow.voteAverage.toString()

            tvShow.posterPath?.let {
                it.loadImage(tvShowImage)
            }

            itemView.setOnClickListener {
                clickItem(tvShow.id)
            }
        }

        companion object {
            fun create(parent: ViewGroup): TvShowViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.tv_show_card, parent, false)
                return TvShowViewHolder(view)
            }
        }
    }

    class TvShowComparator : DiffUtil.ItemCallback<TvShow>() {
        override fun areItemsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: TvShow, newItem: TvShow): Boolean {
            return oldItem == newItem
        }

    }

}