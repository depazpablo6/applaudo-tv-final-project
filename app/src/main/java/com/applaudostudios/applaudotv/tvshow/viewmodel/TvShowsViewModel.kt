package com.applaudostudios.applaudotv.tvshow.viewmodel

import androidx.lifecycle.*
import androidx.paging.PagedList
import com.applaudostudios.domain.login.interaction.DeleteSessionUseCase
import com.applaudostudios.domain.tvshow.interaction.GetTvShowsUseCase
import com.applaudostudios.domain.tvshow.model.TvShow
import com.applaudostudios.domain.tvshow.util.TvShowFilter
import kotlinx.coroutines.launch

class TvShowsViewModel(
        private val getTvShowsUseCase: GetTvShowsUseCase<LiveData<PagedList<TvShow>>>,
        private val deleteSessionUseCase: DeleteSessionUseCase
) :
        ViewModel() {

    private lateinit var tvShows: LiveData<PagedList<TvShow>>
    val tvShowsMutable = MutableLiveData<PagedList<TvShow>>()

    val mediator = MediatorLiveData<Unit>()

    fun filterTvShows(filter: TvShowFilter) {
        viewModelScope.launch {
            tvShows = getTvShowsUseCase(filter)
            mediator.addSource(tvShows) { tvShowsMutable.value = it }
        }
    }

    fun deleteSession() {
        viewModelScope.launch {
            deleteSessionUseCase()
        }
    }

}