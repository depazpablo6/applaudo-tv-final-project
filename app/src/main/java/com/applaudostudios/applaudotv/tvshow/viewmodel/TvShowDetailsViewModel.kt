package com.applaudostudios.applaudotv.tvshow.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.applaudostudios.domain.tvshow.interaction.GetTvShowDetailsUseCase
import com.applaudostudios.domain.tvshow.interaction.IsFavoriteUseCase
import com.applaudostudios.domain.tvshow.interaction.MarkAsFavoriteUseCase
import com.applaudostudios.domain.tvshow.model.TvShowDetails
import kotlinx.coroutines.launch

class TvShowDetailsViewModel(
        private val getTvShowDetailsUseCase: GetTvShowDetailsUseCase,
        private val markAsFavoriteUseCase: MarkAsFavoriteUseCase,
        private val isFavoriteUseCase: IsFavoriteUseCase) :
        ViewModel() {

    val tvShowDetails = MutableLiveData<TvShowDetails>()
    val isFavorite = MutableLiveData<Boolean>()

    fun getTvShowDetails(tvShowId: Int) {
        viewModelScope.launch { tvShowDetails.value = getTvShowDetailsUseCase(tvShowId) }
    }

    fun markAsFavorite(favorite: Boolean) {
        viewModelScope.launch {
            tvShowDetails.value?.let { it.id?.let { it1 -> markAsFavoriteUseCase(it1, favorite) } }
            isFavorite.value = favorite
        }
    }

    fun isFavorite() {
        viewModelScope.launch { isFavorite.value = tvShowDetails.value?.let { it.id?.let { id -> isFavoriteUseCase(id) } } }
    }

}