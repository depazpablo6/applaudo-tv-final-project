package com.applaudostudios.applaudotv.tvshow.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.common.util.loadCircleImage
import com.applaudostudios.domain.tvshow.model.Cast

class CastingAdapter() :
        ListAdapter<Cast, CastingAdapter.CastingViewHolder>(CastingComparator()) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CastingViewHolder {
        return CastingViewHolder.create(parent)
    }

    override fun onBindViewHolder(holder: CastingViewHolder, position: Int) {
        holder.bind(getItem(position))
    }

    class CastingViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private val castingImage: ImageView = itemView.findViewById(R.id.castingImage)
        private val castingName: TextView = itemView.findViewById(R.id.castingName)
        private val castingChar: TextView = itemView.findViewById(R.id.castingChar)

        fun bind(casting: Cast) {

            castingName.text = casting.name
            castingChar.text = casting.character

            casting.profilePath.loadCircleImage(castingImage)

        }

        companion object {
            fun create(parent: ViewGroup): CastingViewHolder {
                val view: View = LayoutInflater.from(parent.context)
                        .inflate(R.layout.casting_card, parent, false)
                return CastingViewHolder(view)
            }
        }
    }

    class CastingComparator : DiffUtil.ItemCallback<Cast>() {
        override fun areItemsTheSame(oldItem: Cast, newItem: Cast): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(oldItem: Cast, newItem: Cast): Boolean {
            return oldItem == newItem
        }

    }

}