package com.applaudostudios.applaudotv.tvshow.ui

import android.content.Intent
import android.graphics.drawable.Drawable
import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.applaudostudios.applaudotv.R
import com.applaudostudios.applaudotv.common.util.loadImage
import com.applaudostudios.applaudotv.databinding.ActivityTvShowDetailsBinding
import com.applaudostudios.applaudotv.season.ui.AllSeasonsActivity
import com.applaudostudios.applaudotv.tvshow.adapter.CastingAdapter
import com.applaudostudios.applaudotv.tvshow.util.IMAGE_BASE_URL
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowDetailsViewModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.SimpleTarget
import com.bumptech.glide.request.transition.Transition
import org.koin.androidx.viewmodel.ext.android.viewModel


@Suppress("DEPRECATION")
class TvShowDetailsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTvShowDetailsBinding
    private val tvShowsDetailsViewModel: TvShowDetailsViewModel by viewModel()
    private lateinit var adapter: CastingAdapter
    private var favorite = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTvShowDetailsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)
        initAdapter()
        val tvShowId = intent.getIntExtra(TV_SHOW_ID, 0)
        tvShowsDetailsViewModel.getTvShowDetails(tvShowId)
        tvShowsDetailsViewModel.tvShowDetails.observe(this, {
            if (it != null) {
                binding.detailsAirDate.text = it.firstAirDate
                binding.detailsAverage.text = it.voteAverage.toString()
                binding.detailsDescription.text = it.overview
                val season = applicationContext.getString(R.string.seasons) + it.lastEpisodeToAir?.seasonNumber
                var creators = applicationContext.getString(R.string.byCreator)
                it.createdBy?.forEach { creator ->
                    creators += creator.name
                    if(it.createdBy?.last() != creator) creators += " / "
                }
                binding.creators.text = creators
                binding.detailsSeasonName.text = season
                binding.detailsTitle.text = it.name
                binding.detailsSeasonAirDate.text = it.seasons?.last()?.airDate

                Glide.with(binding.root)
                        .load(IMAGE_BASE_URL.plus(it.backdropPath))
                        .error(R.drawable.image_error)
                        .centerCrop()
                        .into(object : SimpleTarget<Drawable?>() {
                            override fun onResourceReady(resource: Drawable, transition: Transition<in Drawable?>?) {
                                binding.backgroundDetails.background = resource
                            }
                        })

                it.posterPath?.loadImage(binding.detailsPoster)
                it.seasons?.last()?.posterPath?.loadImage(binding.detailsSeasonImage)

                adapter.submitList(it.cast)
                tvShowsDetailsViewModel.isFavorite()

                tvShowsDetailsViewModel.isFavorite.observe(this, { fav ->
                    favorite = if (fav) {
                        binding.favoriteButton.setImageResource(R.drawable.favorite_solid)
                        fav
                    } else {
                        binding.favoriteButton.setImageResource(R.drawable.favorite_outline)
                        fav
                    }
                    binding.favoriteButton.setOnClickListener {
                        tvShowsDetailsViewModel.markAsFavorite(!favorite)
                    }
                })

                binding.detailsAllSeasons.setOnClickListener {
                    val intent = Intent(this, AllSeasonsActivity::class.java)
                    intent.putExtra(TV_SHOW_ID, tvShowId)
                    startActivity(intent)
                }
            } else {

                Toast.makeText(applicationContext, getString(R.string.networkError), Toast.LENGTH_LONG).show()

                binding.favoriteButton.setOnClickListener {
                    Toast.makeText(applicationContext, getString(R.string.networkError), Toast.LENGTH_LONG).show()
                }
                binding.detailsAllSeasons.setOnClickListener {
                    Toast.makeText(applicationContext, getString(R.string.networkError), Toast.LENGTH_LONG).show()
                }
            }
        })
    }

    private fun initAdapter() {
        adapter = CastingAdapter()
        val castingRecycler = binding.detailsCasting
        castingRecycler.adapter = adapter
        castingRecycler.layoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)
    }


    companion object {
        const val TV_SHOW_ID = "TV_SHOW_ID"
    }

}