package com.applaudostudios.applaudotv.tvshow.ui

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.DividerItemDecoration
import com.applaudostudios.applaudotv.account.ui.AccountActivity
import com.applaudostudios.applaudotv.databinding.ActivityTvShowsBinding
import com.applaudostudios.applaudotv.login.ui.MainActivity
import com.applaudostudios.applaudotv.tvshow.adapter.TvShowAdapter
import com.applaudostudios.applaudotv.tvshow.ui.TvShowDetailsActivity.Companion.TV_SHOW_ID
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowsViewModel
import com.applaudostudios.domain.tvshow.util.TvShowFilter
import org.koin.androidx.viewmodel.ext.android.viewModel

class TvShowsActivity : AppCompatActivity() {

    private lateinit var binding: ActivityTvShowsBinding
    private val tvShowsViewModel: TvShowsViewModel by viewModel()
    private lateinit var adapter: TvShowAdapter
    private var filter = TvShowFilter.POPULAR

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityTvShowsBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        val decoration = DividerItemDecoration(this, DividerItemDecoration.VERTICAL)
        binding.tvShowRecycler.addItemDecoration(decoration)

        initAdapter()
        initSpinner()
        initOnClickListeners()

        tvShowsViewModel.mediator.observe(this, {})

        tvShowsViewModel.tvShowsMutable.observe(this, {
            adapter.submitList(it)
            binding.refreshTvShows.isRefreshing = false
        })

        binding.refreshTvShows.setOnRefreshListener {
            tvShowsViewModel.filterTvShows(filter)
        }

    }

    private fun initOnClickListeners() {
        binding.accountButton.setOnClickListener {
            val intent = Intent(applicationContext, AccountActivity::class.java)
            startActivity(intent)
        }
        binding.logoutButton.setOnClickListener {
            tvShowsViewModel.deleteSession()
            val intent = Intent(applicationContext, MainActivity::class.java)
            startActivity(intent)
            finish()
        }
    }

    private fun initAdapter() {
        adapter = TvShowAdapter {
            val intent = Intent(this, TvShowDetailsActivity::class.java).apply {
                putExtra(TV_SHOW_ID, it)
            }
            startActivity(intent)
        }

        binding.tvShowRecycler.adapter = adapter
    }

    private fun initSpinner() {
        binding.filterTvShows.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                when (p2) {
                    0 -> {
                        filter = TvShowFilter.POPULAR
                        tvShowsViewModel.filterTvShows(filter)
                    }
                    1 -> {
                        filter = TvShowFilter.TOP_RATED
                        tvShowsViewModel.filterTvShows(filter)
                    }
                    2 -> {
                        filter = TvShowFilter.ON_AIR
                        tvShowsViewModel.filterTvShows(filter)
                    }
                    else -> {
                        filter = TvShowFilter.AIRING_TODAY
                        tvShowsViewModel.filterTvShows(filter)
                    }
                }
            }

            override fun onNothingSelected(p0: AdapterView<*>?) {
                return
            }

        }
    }

}