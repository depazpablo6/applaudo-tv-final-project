package com.applaudostudios.applaudotv.login

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.applaudostudios.applaudotv.TestCoroutineRule
import com.applaudostudios.applaudotv.login.viewmodel.MainViewModel
import com.applaudostudios.domain.login.interaction.GetSessionIdUseCase
import com.applaudostudios.domain.login.interaction.RequestSessionIdUseCase
import com.applaudostudios.domain.login.interaction.RequestTokenUseCase
import com.applaudostudios.domain.login.model.RequestToken
import com.applaudostudios.domain.login.model.SessionId
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class LoginViewModelTest {

    private val requestTokenUseCase = mock(RequestTokenUseCase::class.java)
    private val requestSessionIdUseCase = mock(RequestSessionIdUseCase::class.java)
    private val getSessionIdUseCase = mock(GetSessionIdUseCase::class.java)
    private lateinit var loginViewModel: MainViewModel

    private val requestToken = RequestToken(true, "expiresAt", "requestToken")
    private val sessionId = SessionId(true, "sessionId")

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun mockBehavior(){
        loginViewModel = MainViewModel(requestTokenUseCase, requestSessionIdUseCase, getSessionIdUseCase)
        runBlocking {
            `when`(requestTokenUseCase()).thenReturn(requestToken)
            `when`(requestSessionIdUseCase("requestToken")).thenReturn(true)
            `when`(getSessionIdUseCase()).thenReturn(sessionId)
        }
    }

    @Test
    fun getRequestToken_completed_returnRequestToken(){
        testCoroutineRule.runBlockingTest { loginViewModel.getRequestToken() }
        assertThat(loginViewModel.requestToken.value, IsEqual(requestToken))
    }

    @Test
    fun requestSessionId_completed_returnSessionId(){
        testCoroutineRule.runBlockingTest {
            loginViewModel.requestSessionId()
        }
        assertThat(loginViewModel.sessionId.value, IsEqual(sessionId))
    }

}