package com.applaudostudios.applaudotv.account

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.applaudostudios.applaudotv.TestCoroutineRule
import com.applaudostudios.applaudotv.account.viewmodel.AccountViewModel
import com.applaudostudios.domain.account.interaction.GetAccountInfoUseCase
import com.applaudostudios.domain.account.interaction.GetFavoritesUseCase
import com.applaudostudios.domain.account.model.Account
import com.applaudostudios.domain.account.model.Favorite
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

class AccountViewModelTest {

    private val getAccountInfoUseCase = mock(GetAccountInfoUseCase::class.java)
    private val getFavoritesUseCase: GetFavoritesUseCase<LiveData<PagedList<Favorite>>> =
        mock(GetFavoritesUseCase::class.java) as GetFavoritesUseCase<LiveData<PagedList<Favorite>>>
    private lateinit var accountViewModel: AccountViewModel

    private val accountTest = Account(1, "name", "username", "path", "hash")

    private val favorite1 = Favorite("airDate", 1, "name", "overview", "path", 10.0)
    private val favorite2 = Favorite("airDate", 1, "name", "overview", "path", 10.0)
    private val liveData = listOf(favorite1, favorite2)

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()


    @Before
    fun mockBehavior(){

        accountViewModel = AccountViewModel(getAccountInfoUseCase, getFavoritesUseCase)

        runBlocking {
            `when`(getAccountInfoUseCase()).thenReturn(accountTest)
        }

    }

}