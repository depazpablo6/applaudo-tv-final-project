package com.applaudostudios.applaudotv.tvshow

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.applaudostudios.applaudotv.TestCoroutineRule
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowDetailsViewModel
import com.applaudostudios.domain.tvshow.interaction.GetTvShowDetailsUseCase
import com.applaudostudios.domain.tvshow.interaction.IsFavoriteUseCase
import com.applaudostudios.domain.tvshow.interaction.MarkAsFavoriteUseCase
import com.applaudostudios.domain.tvshow.model.TvShowDetails
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class TvShowDetailsViewModelTest {

    private val tvShowId = 1
    private val getTvShowDetailsUseCase = mock(GetTvShowDetailsUseCase::class.java)
    private val markAsFavoriteUseCase = mock(MarkAsFavoriteUseCase::class.java)
    private val isFavoriteUseCase = mock(IsFavoriteUseCase::class.java)
    private lateinit var tvShowDetailsViewModel: TvShowDetailsViewModel

    private val detailsTest = TvShowDetails(
        "path", null, null, 1, null, "name",
        2, "overview", "path", null, "ended", 10.0,
        null
    )

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun mockBehavior(){
        tvShowDetailsViewModel = TvShowDetailsViewModel(getTvShowDetailsUseCase, markAsFavoriteUseCase, isFavoriteUseCase)
        tvShowDetailsViewModel.tvShowDetails.value = TvShowDetails(
            "path", null, null, 1, null, "name",
            2, "overview", "path", null, "ended", 10.0,
            null
        )
        runBlocking {
            `when`(isFavoriteUseCase(tvShowId)).thenReturn(true)
            `when`(markAsFavoriteUseCase(tvShowId, true)).then {
                kotlin.run { tvShowDetailsViewModel.isFavorite.value = true }
            }
            `when`(getTvShowDetailsUseCase(tvShowId)).thenReturn(detailsTest)
        }
    }

    @Test
    fun isFavorite_completed_returnsBoolean(){
        testCoroutineRule.runBlockingTest {
            tvShowDetailsViewModel.isFavorite()
            assertThat(tvShowDetailsViewModel.isFavorite.value, IsEqual(true))
        }
    }

    @Test
    fun markAsFavorite_completed(){
        testCoroutineRule.runBlockingTest {
            tvShowDetailsViewModel.markAsFavorite(true)
            assertThat(tvShowDetailsViewModel.isFavorite.value, IsEqual(true))
        }
    }

    @Test
    fun getTvShowDetails_completed_returnsTvShowDetails(){
        testCoroutineRule.runBlockingTest {
            tvShowDetailsViewModel.getTvShowDetails(tvShowId)
            assertThat(tvShowDetailsViewModel.tvShowDetails.value, IsEqual(detailsTest))
        }
    }

}