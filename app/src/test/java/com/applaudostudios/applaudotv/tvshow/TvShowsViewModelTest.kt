package com.applaudostudios.applaudotv.tvshow

import androidx.lifecycle.LiveData
import androidx.paging.PagedList
import com.applaudostudios.applaudotv.tvshow.viewmodel.TvShowsViewModel
import com.applaudostudios.domain.login.interaction.DeleteSessionUseCase
import com.applaudostudios.domain.login.model.SessionId
import com.applaudostudios.domain.tvshow.interaction.GetTvShowsUseCase
import com.applaudostudios.domain.tvshow.model.TvShow
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Test
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class TvShowsViewModelTest {

    private val getTvShowsUseCase = mock(GetTvShowsUseCase::class.java) as GetTvShowsUseCase<LiveData<PagedList<TvShow>>>
    private val deleteSessionUseCase =  mock(DeleteSessionUseCase::class.java)
    private lateinit var tvShowsViewModel: TvShowsViewModel

    private val sessionId = SessionId(true, "sessionId")
    private val sessionDatabase = mutableListOf(sessionId)

    @Before
    fun mockBehavior(){
        tvShowsViewModel = TvShowsViewModel(getTvShowsUseCase, deleteSessionUseCase)
        runBlocking {
            `when`(deleteSessionUseCase()).then { sessionDatabase.clear() }
        }
    }

    @Test
    fun deleteSession_completed(){
        runBlocking {
            deleteSessionUseCase()
            assertThat(sessionDatabase.isEmpty(), IsEqual(true))
        }
    }

}