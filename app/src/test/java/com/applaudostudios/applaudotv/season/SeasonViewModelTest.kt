package com.applaudostudios.applaudotv.season

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import com.applaudostudios.applaudotv.TestCoroutineRule
import com.applaudostudios.applaudotv.season.viewmodel.SeasonViewModel
import com.applaudostudios.domain.season.interaction.GetAllSeasonsUseCase
import com.applaudostudios.domain.season.model.Episode
import com.applaudostudios.domain.season.model.SeasonWithEpisodes
import kotlinx.coroutines.runBlocking
import org.hamcrest.MatcherAssert.assertThat
import org.hamcrest.core.IsEqual
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.rules.TestRule
import org.mockito.Mockito.`when`
import org.mockito.Mockito.mock

internal class SeasonViewModelTest {

    private val tvShowId = 1
    private val getAllSeasonsUseCase = mock(GetAllSeasonsUseCase::class.java)
    private lateinit var seasonViewModel: SeasonViewModel

    private val episodes = Episode(
        "airDate",
        1,
        1,
        "name",
        "overview",
        "path",
        10.0,
        1,
        tvShowId
    )
    private val episodeList = listOf(episodes)
    private val seasonWithEpisode = SeasonWithEpisodes(1, episodeList)
    private val seasonsList = listOf(seasonWithEpisode)

    @get:Rule
    val testInstantTaskExecutorRule: TestRule = InstantTaskExecutorRule()

    @get:Rule
    val testCoroutineRule = TestCoroutineRule()

    @Before
    fun mockBehavior(){
        seasonViewModel = SeasonViewModel(getAllSeasonsUseCase)
        runBlocking {
            `when`(getAllSeasonsUseCase(tvShowId)).thenReturn(seasonsList)
        }
    }

    @Test
    fun getAllSeasons_completed_returnSeasonWithEpisodes(){
        testCoroutineRule.runBlockingTest {
            seasonViewModel.getAllSeasons(tvShowId)
        }
        assertThat(seasonViewModel.allSeasons.value, IsEqual(seasonsList))
    }

}